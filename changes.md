Net4Care CDA Builder Releases
------------------------------
CDA builders 2.0.0:
 * STDC precondtions support in QFDD builder and parser
 * Performance optimizations
 * A lot of restructuring
 
CDA Tools 1.2.0:
 * New commandline tool: CDA downloader
 * Support for cron notation in test dcouments generators
 * A lot of restructuring
------------------------------
 CDA Tools 1.0.1:
 * Updated dependency to XDSConnector version 0.1.0
 
 1.0.0:
 * CDA -> XML conversion complete
 * XML -> CDA conversion complete
 * Support for narratives in section text elements (must be correct xhtml)
 * A lot of restructuring
 
 0.0.11-SNAPSHOT:
 * XML -> PHMR conversion
 * Generel code cleanup
  
 0.0.10:
  * Inital XML -> PHMR conversion

 0.0.9:
  * Text elements now supports non-escaped values (Needed for embedding HTML) 
  
 0.0.8:
  * PHMR builder matches DK profile (V1.2)
  * QRD builder matches DK profile (V1.1)
  * QFDD builder matches DK profile (V1.1)
  * Testdata generators for generating CDA xml files
  * CDAUplader for uploading a folder of CDA xml files to an XDS Repository
 
 0.0.8-APLHA:
  * Replace openConnect with raw xml building (better performance)
  * New structure for code
  * Update PHMR builder to match latest DK profile (V1.1)
  * Add QRD builder (DK profile v1.0)
  * QFDD builder is work in progress
  * Testdata generation is work in progress

 0.0.7:
  * Enabled deployment on 4S artifactory.

 0.0.6:
  * Added (lots) to support simple QFDD building. This entailed
    quite a lot of abstraction in the PHMR builder, as well
    as adding several new modules and classes.
  * Updated PHMR examples to have the new MedCom OIDs.
  * NOTE: The QFDD model and building is NOT COMPLETE. It is provided
     as a starting point for further work. Refactoring in the
     module structure also pending.

 0.0.5: 
  * Added toString to several SimpleClinicalDocument part objects.
 
 0.0.4: 
  * Clean up of builder interface.

 0.0.3: 
  * Changed MedicalEquipment to use Bloch builder pattern.
  
 0.0.2: 
  * Initial release (September 2014).
 
