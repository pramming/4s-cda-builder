The CDA builder validator is intended for validating the CDA Builder project. The validation is done at build time hence the created binary has no real usage.

The project dependents on 4s-cda-builders and cda-client-validator.