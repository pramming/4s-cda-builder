package goimplement.it;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import generated.CdaType;
import goimplement.it.Hl7JkiddoRhcloudCom_Service.CDA;
import goimplement.it.ValidationResponse;

public class ShowUsage {

	public static void main(final String[] args) throws MalformedURLException, IOException {

		final CDA cdaValidationClient = ClientBuilder.newClient();
		ValidationResponse result = null;
		
		
		result = cdaValidationClient.validateCdaType(CdaType.PHMR.name()).postXmlAsJson("", ValidationResponse.class);
		System.out.println(result);

		
		final String document = Resources.toString(new URL("file:src/main/resources/QRDOC.sample.xml"), Charsets.UTF_8);
		result = cdaValidationClient.validateCdaType(CdaType.PHMR.name()).postXmlAsJson(document,
				ValidationResponse.class);
		System.out.println(result);
	}
}
