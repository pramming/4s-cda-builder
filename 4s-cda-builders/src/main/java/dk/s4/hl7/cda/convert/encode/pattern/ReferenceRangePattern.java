package dk.s4.hl7.cda.convert.encode.pattern;

import java.io.IOException;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ReferenceRange;
import dk.s4.hl7.cda.model.qfdd.QFDDNumericQuestion;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

public class ReferenceRangePattern {
  public void build(QRDNumericResponse qrdNumericResponse, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qrdNumericResponse != null && qrdNumericResponse.getIntervalType() != null
        && (qrdNumericResponse.getMinimum() != null || qrdNumericResponse.getMaximum() != null)) {
      build(qrdNumericResponse.getMinimum(), qrdNumericResponse.getMaximum(),
          qrdNumericResponse.getIntervalTypeAsString(), xmlBuilder);
    }
  }

  public void build(QFDDNumericQuestion qrdNumericResponse, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qrdNumericResponse != null && qrdNumericResponse.getIntervalType() != null
        && (qrdNumericResponse.getMinimum() != null || qrdNumericResponse.getMaximum() != null)) {
      build(qrdNumericResponse.getMinimum(), qrdNumericResponse.getMaximum(),
          qrdNumericResponse.getIntervalTypeAsString(), xmlBuilder);
    }
  }

  public void build(List<ReferenceRange> observationRanges, XmlStreamBuilder xmlBuilder) throws IOException {
    String intervalType = "IVL_PQ";
    for (ReferenceRange range : observationRanges) {
      build(MedCom.DK_PHMR_OBSERVATION_RANGE_ROOT_OID, range.getCode(), range.getLowValue(),
          range.isLowValueInclusive(), range.getHighValue(), range.isHighValueInclusive(), intervalType, xmlBuilder);
    }
  }

  private void build(String minimum, String maximum, String intervalType, XmlStreamBuilder xmlBuilder)
      throws IOException {
    xmlBuilder.element("referenceRange").attribute("typeCode", "REFV");
    BuildUtil.buildTemplateIds(xmlBuilder, HL7.RESPONSE_REFERENCE_RANGE_PATTERN_OID);
    xmlBuilder.element("observationRange");

    xmlBuilder.element("value").attribute("xsi:type", intervalType);
    if (minimum != null) {
      xmlBuilder.element("low").attribute("value", minimum).elementShortEnd();
    }
    if (maximum != null) {
      xmlBuilder.element("high").attribute("value", maximum).elementShortEnd();
    }
    xmlBuilder.elementEnd(); // end value
    xmlBuilder.elementEnd(); // end observationRange
    xmlBuilder.elementEnd(); // end referenceRange
  }

  private void build(String templateId, CodedValue code, String lowValue, boolean lowValueInclusive, String highValue,
      boolean highValueInclusive, String intervalType, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("referenceRange");
    xmlBuilder.element("observationRange").attribute("classCode", "OBS").attribute("moodCode", "EVN.CRT");
    BuildUtil.buildTemplateIds(xmlBuilder, templateId);
    BuildUtil.buildCode(code, xmlBuilder);
    if (lowValue != null || highValue != null) {
      xmlBuilder.element("value").attribute("xsi:type", intervalType);
      buildIntervalBoundary("low", lowValue, lowValueInclusive, xmlBuilder);
      buildIntervalBoundary("high", highValue, highValueInclusive, xmlBuilder);
      xmlBuilder.elementEnd(); // end value
    }
    xmlBuilder.elementEnd(); // end observationRange
    xmlBuilder.elementEnd(); // end referenceRange
  }

  private static void buildIntervalBoundary(String elementName, String value, boolean inclusive,
      XmlStreamBuilder xmlBuilder) throws IOException {
    if (value != null) {
      xmlBuilder
          .element(elementName)
          .attribute("value", value)
          .attribute("inclusive", inclusive ? "true" : "false")
          .elementShortEnd();
    } else {
      buildNullFlavor(elementName, xmlBuilder);
    }
  }

  private static void buildNullFlavor(String elementName, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element(elementName).attribute("nullFlavor", "NA").elementShortEnd();
  }

}
