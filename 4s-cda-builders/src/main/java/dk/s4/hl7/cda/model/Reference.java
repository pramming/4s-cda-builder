package dk.s4.hl7.cda.model;

/**
 * Models a reference to another CDA document and optionally an observation in
 * that document. The reference also specifies which method should be used to
 * retrieve the referenced document.
 */
public class Reference {

  /**
   * Method for how to retrieve the document
   */
  public static enum DocumentIdReferencesUse {
    CDA_DOCUMENT_ID_REFERENCE("NA"),
    XDS_UNIQUE_ID_REFERENCE("1"),
    LINK_RESOLVER_REFERENCE("2"),
    HTTP_REFERENCE("3");

    private String referencesUse;

    DocumentIdReferencesUse(String referencesUse) {
      this.referencesUse = referencesUse;
    }

    public String getReferencesUse() {
      return referencesUse;
    }
  }

  private String referencesUse; // required
  // reference to external document
  private ID externalDocument; // required
  private CodedValue externalDocumentType; // required

  // Reference to external observation
  private ID externalObservation; // optional

  private Reference(ReferenceBuilder builder) {
    this.referencesUse = builder.referencesUse;
    this.externalDocument = builder.externalDocument;
    this.externalObservation = builder.externalObservation;
    this.externalDocumentType = builder.externalDocumentType;
  }

  public CodedValue getExternalDocumentType() {
    return externalDocumentType;
  }

  public String getReferencesUse() {
    return referencesUse;
  }

  public ID getExternalDocument() {
    return externalDocument;
  }

  public ID getExternalObservation() {
    return externalObservation;
  }

  public static class ReferenceBuilder {
    private CodedValue externalDocumentType;
    private String referencesUse;
    private ID externalDocument;
    private ID externalObservation;

    public ReferenceBuilder(String referencesUse, ID externalDocument, CodedValue externalDocumentType) {
      this.referencesUse = referencesUse;
      this.externalDocument = externalDocument;
      this.externalDocumentType = externalDocumentType;
    }

    public ReferenceBuilder externalObservation(ID externalObservation) {
      this.externalObservation = externalObservation;
      return this;
    }

    public Reference build() {
      return new Reference(this);
    }
  }
}
