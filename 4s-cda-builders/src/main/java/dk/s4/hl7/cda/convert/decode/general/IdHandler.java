package dk.s4.hl7.cda.convert.decode.general;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

/**
 * General Id handler. Hence the
 * XPath for which this handler is called is configurable
 */
public class IdHandler extends BaseXmlHandler {
  private static final String ID_ELEMENT_NAME = "id";
  private ID id;

  public IdHandler(String xpathToParentElement) {
    addPath(xpathToParentElement + "/" + ID_ELEMENT_NAME);
  }

  public ID getId() {
    return id;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), ID_ELEMENT_NAME, "root",
        "extension")) {
      id = new ID.IDBuilder()
          .setAuthorityName(xmlElement.getAttributeValue("assigningAuthorityName"))
          .setExtension(xmlElement.getAttributeValue("extension"))
          .setRoot(xmlElement.getAttributeValue("root"))
          .build();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

  public void clear() {
    id = null;
  }
}
