package dk.s4.hl7.cda.model.qrd;

import dk.s4.hl7.cda.model.util.ModelUtil;

/**
 * Simple text response where the user has provided an answer as text
 */
public class QRDTextResponse extends QRDResponse {
  private String text;

  /**
   * "Effective Java" Builder for constructing QRDTextResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QRDTextResponseBuilder
      extends QRDResponse.BaseQRDResponseBuilder<QRDTextResponse, QRDTextResponseBuilder> {
    public QRDTextResponseBuilder() {
    }

    private String text;

    public QRDTextResponseBuilder setText(String text) {
      ModelUtil.checkNull(text, "Text is mandatory");
      this.text = text;
      return this;
    }

    public QRDTextResponse build() {
      ModelUtil.checkNull(text, "Text is mandatory");
      return new QRDTextResponse(this);
    }

    @Override
    public QRDTextResponseBuilder getThis() {
      return this;
    }
  }

  private QRDTextResponse(QRDTextResponseBuilder builder) {
    super(builder);
    text = builder.text;
  }

  public String getText() {
    return text;
  }
}
