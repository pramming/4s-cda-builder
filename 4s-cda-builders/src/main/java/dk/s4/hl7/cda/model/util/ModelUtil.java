package dk.s4.hl7.cda.model.util;

import dk.s4.hl7.cda.codes.IntervalType;

/**
 * General utilities used in the greenCDA model classes
 */
public class ModelUtil {
  private static final String NULL_FLAVOR_VALUE = "NI";

  public static boolean isTextUseful(String text) {
    if (text != null) {
      text = text.trim();
      return !text.isEmpty() && !text.equalsIgnoreCase("null") && !text.equalsIgnoreCase(NULL_FLAVOR_VALUE);
    }
    return false;
  }

  public static void checkNull(Object text, String message) {
    if (text == null) {
      throw new IllegalArgumentException(message);
    }
  }

  public static void checkNull(String text, String message) {
    if (!isTextUseful(text)) {
      throw new IllegalArgumentException(message);
    }
  }

  public static String checkDots(String value, IntervalType intervalType) {
    if (intervalType == IntervalType.IVL_REAL) {
      return value.replace(',', '.');
    }
    return value;
  }
}
