package dk.s4.hl7.cda.convert.decode.qfdd;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.model.qfdd.QFDDHelpText;
import dk.s4.hl7.cda.model.qfdd.QFDDHelpText.QFDDHelpTextBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class HelpTextHandler extends BaseXmlHandler {
  public static final String HELPTEXT_BASE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/entryRelationship/observation";
  private String helpText;
  private String language;

  public HelpTextHandler() {
    addPath(HELPTEXT_BASE + "/value");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "language")) {
        language = xmlElement.getAttributeValue("language");
      }
      if ("ST".equalsIgnoreCase(xmlElement.getAttributeValue("type"))) {
        helpText = xmlElement.getElementValue();
      }
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignored
  }

  public QFDDHelpText getHelpText() {
    if (hasValues()) {
      return new QFDDHelpTextBuilder().helpText(helpText).language(language).build();
    }
    return null;
  }

  private boolean hasValues() {
    // Language is optional
    return helpText != null;
  }

  public void clear() {
    helpText = null;
    language = null;
  }
}
