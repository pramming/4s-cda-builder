package dk.s4.hl7.cda.convert.decode.general;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.util.xml.XmlHandler;
import dk.s4.hl7.util.xml.XmlMapping;

/**
 * 
 * Base XmlHandler implementation that adds basic add/remove handler
 * functionality to implementors
 *
 */
public abstract class BaseXmlHandler implements XmlHandler {
  protected List<String> paths;

  public BaseXmlHandler() {
    this.paths = new ArrayList<String>();
  }

  protected void addPath(String path) {
    this.paths.add(path);
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    for (String path : paths) {
      xmlMapping.add(path, this);
    }
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    for (String path : paths) {
      xmlMapping.remove(path);
    }
  }
}
