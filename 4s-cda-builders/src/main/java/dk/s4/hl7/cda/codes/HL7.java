package dk.s4.hl7.cda.codes;

import dk.s4.hl7.cda.model.CodedValue;

//CHECKSTYLE:OFF
public class HL7 {

  // International codes
  public static final String CDA_TYPEID_EXTENSION = "POCD_HD000040";
  public static final String CDA_TYPEID_ROOT = "2.16.840.1.113883.1.3";

  public static final String CONFIDENTIALITY_OID = "2.16.840.1.113883.5.25";
  public static final String GENDER_OID = "2.16.840.1.113883.5.1";

  public static final String OBSERVATION_ORGANIZER = "2.16.840.1.113883.10.20.1.35";
  public static final String OBSERVATION = "2.16.840.1.113883.10.20.1.31";
  public static final String DEVICE_DEFINITION_ORGANIZER = "2.16.840.1.113883.10.20.9.4";
  public static final String PHMR_NUMERIC_OBSERVATION = "2.16.840.1.113883.10.20.9.8";
  public static final String PHMR_PRODUCT_INSTANCE = "2.16.840.1.113883.10.20.9.9";
  public static final String PRODUCT_INSTANCE_TEMPLATE_OID = "2.16.840.1.113883.10.20.1.52";

  public static final String PHMR_VITAL_SIGNS_FIRST_ID = "2.16.840.1.113883.10.20.1.16";
  public static final String PHMR_VITAL_SIGNS_SECOND_ID = "2.16.840.1.113883.10.20.9.2";
  public static final String PHMR_VITAL_SIGNS_THIRD_ID = "1.2.208.184.11.1";

  public static final String PHMR_RESULTS_FIRST_ID = "2.16.840.1.113883.10.20.1.14";
  public static final String PHMR_RESULTS_SECOND_ID = "2.16.840.1.113883.10.20.9.14";
  public static final String PHMR_RESULTS_THIRD_ID = "1.2.208.184.11.1";

  public static final String PHMR_MEDICAL_EQUIPMENT_FIRST_ID = "2.16.840.1.113883.10.20.1.7";
  public static final String PHMR_MEDICAL_EQUIPMENT_SECOND_ID = "2.16.840.1.113883.10.20.9.1";
  public static final String PHMR_MEDICAL_EQUIPMENT_THIRD_ID = "1.2.208.184.11.1";

  public static final String PHMR_ENTRY_ID = "1.2.208.184.11.1";

  public static final String QFDD_SECTION_ROOT_OID = "2.16.840.1.113883.10.20.32.2.1";
  public static final String QFDD_QUESTION_ORGANIZER = "2.16.840.1.113883.10.20.32.4.1";
  public static final String QFDD_QUESTION_MEDIA_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.2";
  public static final String QFDD_CRITERION_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.3";
  public static final String QFDD_PRECONDITION_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.4";
  public static final String QFDD_SDTC_PRECONDITION_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.12";
  public static final String QFDD_QUESTION_HELP_TEXT_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.19";

  public static final String QFDD_QUESTION_FEEDBACK_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.6";
  public static final String QFDD_COPYRIGHT_SECTION_TEMPLATEID = "2.16.840.1.113883.10.20.32.2.2";
  public static final String QFDD_COPYRIGHT_PATTERN_TEMPLATEID = "2.16.840.1.113883.10.20.32.4.21";
  public static final String QFDD_NUMERIC_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.7";
  public static final String QFDD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.8";
  public static final String QFDD_TEXT_QUESTION_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.9";
  public static final String QFDD_ANALOG_SLIDER_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.10";
  public static final String QFDD_DISCRETE_SLIDER_PATTERN_OID = "2.16.840.1.113883.10.20.32.4.11";

  public static final String QRD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID = "2.16.840.1.113883.10.20.33.4.5";
  public static final String QRD_TEXT_QUESTION_PATTERN_OID = "2.16.840.1.113883.10.20.33.4.6";
  public static final String QRD_DISCRETE_SLIDER_PATTERN_OID = "2.16.840.1.113883.10.20.33.4.8";
  public static final String QRD_ANALOG_SLIDER_PATTERN_OID = "2.16.840.1.113883.10.20.33.4.7";
  public static final String QRD_NUMERIC_PATTERN_OID = "2.16.840.1.113883.10.20.33.4.4";
  public static final String QRD_RESPONSE_PATTERN_OID = "2.16.840.1.113883.10.20.33.4.1";
  public static final String QRD_SECTION_ID = "2.16.840.1.113883.10.20.33.2.1";
  public static final String QRD_ENTRY_ID = "2.16.840.1.113883.10.20.33.2.1";

  public static final String QUESTION_OPTIONS_PATTERN_OBSERVATION_OID = "2.16.840.1.113883.10.20.32.4.20";
  public static final String RESPONSE_REFERENCE_RANGE_PATTERN_OID = "2.16.840.1.113883.10.20.33.4.3";

  public static final String REALM_CODE_DK = "DK";

  public static final String APD_ID = "1.2.208.184.14.11.1";
  public static final String APD_ID_EXTENSION = "2017-03-10";

  public static final String APD_ENCOUNTER_ID = "1.2.208.184.14.11.2";
  public static final String APD_LOCATION_ID = "1.2.208.184.14.11.3";

  public static final CodedValue SELF_REFERENCE = new CodedValue.CodedValueBuilder()
      .setCode("SELF")
      .setCodeSystem("2.16.840.1.113883.5.111")
      .setDisplayName("Self")
      .setCodeSystemName("HL7 Role code")
      .build();

}
