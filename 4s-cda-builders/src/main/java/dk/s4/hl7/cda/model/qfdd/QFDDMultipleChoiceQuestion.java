package dk.s4.hl7.cda.model.qfdd;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.model.CodedValue;

/**
 * Multiple choice where the user must provide 0 to many answers from a list of
 * possible answers.
 * 
 * The number of answers allowed is controlled by minimum and maximum.
 * The list of possible answers is set as answer options
 */
public class QFDDMultipleChoiceQuestion extends QFDDQuestion {
  private static final Logger logger = LoggerFactory.getLogger(QFDDMultipleChoiceQuestionBuilder.class);
  private List<CodedValue> answerOptionList = new ArrayList<CodedValue>();
  private int minimum; // low
  private int maximum; // high

  /**
   * "Effective Java" Builder for constructing QFDDNumericResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */

  public static class QFDDMultipleChoiceQuestionBuilder
      extends QFDDQuestion.BaseQFDDQuestionBuilder<QFDDMultipleChoiceQuestion, QFDDMultipleChoiceQuestionBuilder> {

    private List<CodedValue> answerOptionList = new ArrayList<CodedValue>();
    private int minimum = -1; // low
    private int maximum = -1; // high

    public QFDDMultipleChoiceQuestionBuilder setInterval(int minimum, int maximum) {
      this.minimum = minimum;
      this.maximum = maximum;
      return this;
    }

    public QFDDMultipleChoiceQuestion build() {
      validateBounds();
      return new QFDDMultipleChoiceQuestion(this);
    }

    private void validateBounds() {
      if (minimum < 0) {
        logger.warn("The minimum must be 0 or more but was: " + minimum + ". Defaulting to 0");
        minimum = 0;
      }
      if (maximum < 1) {
        logger.warn("The maximum must be 1 or more but was: " + maximum + ". Defaulting to 1");
        maximum = 1;
      }
      if (minimum > maximum) {
        logger.warn(String.format("Minimum is larger than maximum: [min:%s > max:%s]. Switching the two values",
            minimum, maximum));
        int temp = minimum;
        minimum = maximum;
        maximum = temp;
      }
    }

    public QFDDMultipleChoiceQuestionBuilder addAnswerOption(String code, String codeSystem, String displayName,
        String codeSystemName) {
      answerOptionList.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
      return this;
    }

    @Override
    public QFDDMultipleChoiceQuestionBuilder getThis() {
      return this;
    }
  }

  private QFDDMultipleChoiceQuestion(QFDDMultipleChoiceQuestionBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    maximum = builder.maximum;
    answerOptionList = builder.answerOptionList;
  }

  @Override
  public String toString() {
    return getQuestion();
  }

  public void addAnswerOption(String code, String codeSystem, String displayName, String codeSystemName) {
    answerOptionList.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
  }

  public List<CodedValue> getAnswerOptionList() {
    return answerOptionList;
  }

  public int getMinimum() {
    return minimum;
  }

  public int getMaximum() {
    return maximum;
  }
}
