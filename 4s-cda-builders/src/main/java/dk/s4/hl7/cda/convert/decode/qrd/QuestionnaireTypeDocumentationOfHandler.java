package dk.s4.hl7.cda.convert.decode.qrd;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class QuestionnaireTypeDocumentationOfHandler implements CDAXmlHandler<QRDDocument> {
  private CodedValue questionnaireType;

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "codeSystem")) {
      questionnaireType = new CodedValue.CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .build();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/code", this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/code");
  }

  @Override
  public void addDataToDocument(QRDDocument qrdDocument) {
    qrdDocument.setQuestionnaireType(questionnaireType);
  }
}
