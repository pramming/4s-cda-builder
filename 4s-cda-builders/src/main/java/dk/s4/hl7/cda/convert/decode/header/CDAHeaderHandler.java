package dk.s4.hl7.cda.convert.decode.header;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class CDAHeaderHandler implements CDAXmlHandler<ClinicalDocument> {
  private static Logger logger = LoggerFactory.getLogger(CDAHeaderHandler.class);
  private String title;
  private String effectiveTime;
  private ID id;
  private String code;
  private String language;
  private String setId;
  private String versionNumber;
  private List<String> templateIds;

  public CDAHeaderHandler() {
    this.templateIds = new ArrayList<String>();
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    String elementName = xmlElement.getElementName();
    if (ConvertXmlUtil.isElementValuePresent(xmlElement, elementName, "title")) {
      title = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, elementName, "effectiveTime", "value")) {
      effectiveTime = xmlElement.getAttributeValue("value");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, elementName, "id", "root", "extension")) {
      id = new ID.IDBuilder()
          .setAuthorityName(xmlElement.getAttributeValue("assigningAuthorityName"))
          .setExtension(xmlElement.getAttributeValue("extension"))
          .setRoot(xmlElement.getAttributeValue("root"))
          .build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, elementName, "templateId", "root")) {
      templateIds.add(xmlElement.getAttributeValue("root"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, elementName, "languageCode", "code")) {
      language = xmlElement.getAttributeValue("code");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, elementName, "code", "code")) {
      code = xmlElement.getAttributeValue("code");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, elementName, "versionNumber", "value")) {
      versionNumber = xmlElement.getAttributeValue("value");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, elementName, "setId", "root", "extension")) {
      setId = xmlElement.getAttributeValue("extension");
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add("/ClinicalDocument/title", this);
    xmlMapping.add("/ClinicalDocument/effectiveTime", this);
    xmlMapping.add("/ClinicalDocument/id", this);
    xmlMapping.add("/ClinicalDocument/templateId", this);
    xmlMapping.add("/ClinicalDocument/code", this);
    xmlMapping.add("/ClinicalDocument/languageCode", this);
    xmlMapping.add("/ClinicalDocument/versionNumber", this);
    xmlMapping.add("/ClinicalDocument/setId", this);
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    // Ignore
  }

  public ID getDocumentId() {
    return id;
  }

  @Override
  public void addDataToDocument(ClinicalDocument clinicalDocument) {
    matchTemplateIds(clinicalDocument);
    clinicalDocument.setTitle(title);
    clinicalDocument.setLanguageCode(language);
    clinicalDocument.setDocumentVersion(setId, toInteger(versionNumber));
    if (!clinicalDocument.getCode().equals(code)) {
      logger.warn("Document has unexpected document code/type: " + code);
    }
    clinicalDocument.setEffectiveTime(ConvertXmlUtil.getDateFromyyyyMMddhhmmss(effectiveTime));
  }

  private Integer toInteger(String versionNumber) {
    if (versionNumber != null) {
      try {
        return Integer.parseInt(versionNumber);
      } catch (Exception ex) {
        logger.warn("Document version is not an integer value: " + versionNumber);
        return null;
      }
    }
    return null;
  }

  private void matchTemplateIds(ClinicalDocument clinicalDocument) {
    for (String actualTemplateId : templateIds) {
      boolean found = false;
      for (String expectedTemplateId : clinicalDocument.getTemplateIds()) {
        if (expectedTemplateId.equals(actualTemplateId)) {
          found = true;
          break;
        }
      }
      if (!found) {
        logger.warn("CDA document has unexpected template document id: " + actualTemplateId);
      }
    }
  }
}