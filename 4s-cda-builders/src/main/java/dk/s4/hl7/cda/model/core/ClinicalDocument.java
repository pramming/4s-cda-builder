package dk.s4.hl7.cda.model.core;

import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Patient;

/**
 * Base interface for all GreenCDA documents.
 **/
public interface ClinicalDocument {
  /**
   * @return Realm code of the document
   */
  String getRealmCode();

  /**
   * @return Type id root
   */
  String getTypeIdRoot();

  /**
   * @return Type id extension
   */
  String getTypeIdExtension();

  /**
   * @return Template ids of the document
   */
  String[] getTemplateIds();

  /**
   * @return Document id
   */
  ID getId();

  /**
   * @return Document code
   */
  String getCode();

  /**
   * @return Document code display name
   */
  String getCodeDisplayName();

  /**
   * @return Document title
   */
  String getTitle();

  /**
   * Set document title
   * 
   * @param title
   */
  void setTitle(String title);

  /**
   * Define when the document was created.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setEffectiveTime(java.util.Date)
   * @param documentCreationTime
   *          The document creation time.
   */
  void setEffectiveTime(Date now);

  /**
   * Define when the document was created.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setEffectiveTime(java.util.Date)
   * @return documentCreationTime The document creation time.
   */
  Date getEffectiveTime();

  /**
   * Always 'N' normal in DK
   * 
   * @return confidentiality code
   */
  String getConfidentialityCode();

  // TODO: Remove. Hardcoded value to 'N'
  /**
   * @param code
   *          Confidentiality code
   */
  void setConfidentialityCode(String code);

  /**
   * Language code of the document. e.g. da-DK
   * 
   * @return language code
   */
  String getLanguageCode();

  // TODO: Remove. Hardcoded value to da-DK
  /**
   * 
   * @param language
   */
  void setLanguageCode(String language);

  /**
   * Define a unique identifier (CDA Release 2, §4.2.1.7 ClinicalDocument.setId
   * / "Represents an identifier that is common across all document revisions")
   * and the version number (CDA Release 2, §4.2.1.8
   * ClinicalDocument.versionNumber /
   * "An integer value used to version successive replacement documents".
   * Normally for PHMR documents just use a unique identifier, and version
   * number 1.
   * 
   * @param uniqueId
   *          the identifier for the document
   * @param versionNumber
   *          the number of this version
   */
  void setDocumentVersion(String setId, Integer versionNumber);

  /**
   * Gets the unique identifier (CDA Release 2, §4.2.1.7 ClinicalDocument.setId)
   * 
   * @return uniqueId the identifier for the document
   */
  String getUniqueId();

  /**
   * Gets version number (CDA Release 2, §4.2.1.8
   * ClinicalDocument.versionNumber)
   * 
   * @return versionNumber the number of this version
   */
  Integer getVersionNumber();

  /**
   * Define who is the patient.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setPatientRole(org.net4care.phmr.test.model.PersonIdentity)
   * @param patientIdentity
   *          The patient identity as a PersonIdentity.
   * 
   */
  void setPatient(Patient patientIdentity);

  /**
   * Returns the patient.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setPatientRole(org.net4care.phmr.test.model.PersonIdentity)
   * @return patientIdentity The patient identity as a PersonIdentity.
   * 
   */
  Patient getPatient();

  /**
   * Define who is the author of the document, i.e. the organization and person
   * and time when the author participated in creating the document.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setAuthor(org.net4care.phmr.test.model.PersonIdentity,
   *      java.util.Date)
   * @param authorOrganizationIdentity
   *          The author organization identity.
   * @param authorPersonIdentity
   *          The author person identity.
   * @param authorParticipationStart
   *          The author participation start.
   */
  void setAuthor(Participant Author);

  Participant getAuthor();

  /**
   * Represents a participant who transferred the content
   * 
   * @param dataEnterer Participant who transferred the content
   */
  void setDataEnterer(Participant dataEnterer);

  Participant getDataEnterer();

  /**
   * Set the organization that is custodian of the document.
   * 
   * @param custodianIdentity
   *          The custodian organization identity.
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setCustodian(org.net4care.phmr.test.model.OrganizationIdentity)
   */
  void setCustodian(OrganizationIdentity custodianIdentity);

  OrganizationIdentity getCustodianIdentity();

  /**
   * Represents the legal authenticator of the document
   * 
   * @param legalAuthenticator the legal authenticator
   */
  void setLegalAuthenticator(Participant legalAuthenticator);

  Participant getLegalAuthenticator();

  /**
   * Set the time interval that the measurements span.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setDocumentationTime(java.util.Date,
   *      java.util.Date)
   * @param from
   *          The earliest time in the time interval.
   * @param to
   *          The latest time in the time interval.
   */
  void setDocumentationTimeInterval(Date from, Date to);

  /**
   * Get the time start for the measurements span.
   * 
   * @return from The earliest time in the time interval.
   */
  Date getServiceStartTime();

  /**
   * Get the end time for the measurements span.
   * 
   * @return from The latest time in the time interval.
   */
  Date getServiceStopTime();

  List<Participant> getParticipants();

  /**
   * The participants identifies other supporting participants, including
   * parents, relatives, caregivers and other participants related in some way
   * to the patient
   * 
   * @param participants the list of participants
   */
  void setParticipants(List<Participant> participants);

  void addParticipant(Participant participant);

  List<Participant> getInformationRecipients();

  /**
   * The informationRecipients records the intended recipient of the
   * information at the time the document is created. For example, in cases
   * where the intended recipient of the document is the patient's health chart,
   * set the receivedOrganization to be the scoping organization for that chart.
   * 
   * @param participants the list of information recipients
   */
  void setInformationRecipients(List<Participant> participants);

  void addInformationRecipients(Participant participant);

  /**
   * The id of the documentSet for which this version of the document is part of
   * 
   * @return set ID
   */
  String getSetId();

  void setSetId(String setId);
}