package dk.s4.hl7.cda.convert.v11;

import java.util.Collections;
import java.util.List;

import dk.s4.hl7.cda.convert.XmlAPDConverter;
import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.v11.ApdV11SectionHandler;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;

public class XmlAPDV11Converter extends XmlAPDConverter {
  @Override
  protected List<CDAXmlHandler<AppointmentDocument>> createSectionHandlers() {
    CDAXmlHandler<AppointmentDocument> handler = new ApdV11SectionHandler();
    return Collections.singletonList(handler);
  }

  @Override
  protected AppointmentDocument createNewDocument(CDAHeaderHandler headerHandler) {
    return super.createNewDocument(headerHandler);
  }
}
