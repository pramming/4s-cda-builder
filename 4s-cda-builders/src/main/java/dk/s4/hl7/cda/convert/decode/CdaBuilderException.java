package dk.s4.hl7.cda.convert.decode;

/**
 * General CDA builder exception
 */
public class CdaBuilderException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public CdaBuilderException(String message) {
    super(message);
  }

  public CdaBuilderException(Exception exception) {
    super(exception);
  }

  public CdaBuilderException(String message, Exception exception) {
    super(message, exception);
  }
}
