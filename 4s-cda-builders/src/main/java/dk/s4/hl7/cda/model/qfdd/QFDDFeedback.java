package dk.s4.hl7.cda.model.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.util.ModelUtil;

/**
 * Provides feedback to the user depending on one or more preconditions.
 */
public class QFDDFeedback {
  private String language;
  private final String feedBackText;
  private List<QFDDPrecondition> qfddPreconditionList;

  private QFDDFeedback(QFDDFeedbackBuilder builder) {
    this.language = builder.language;
    this.feedBackText = builder.feedBackText;
    this.qfddPreconditionList = builder.qfddPreconditionList;
  }

  public static class QFDDFeedbackBuilder {
    private String language;
    private String feedBackText;
    private List<QFDDPrecondition> qfddPreconditionList;

    public QFDDFeedbackBuilder() {
      this.qfddPreconditionList = new ArrayList<QFDDPrecondition>();
    }

    public QFDDFeedbackBuilder language(String language) {
      this.language = language;
      return this;
    }

    public QFDDFeedbackBuilder feedBackText(String feedBackText1) {
      this.feedBackText = feedBackText1;
      return this;
    }

    public QFDDFeedbackBuilder addPrecondition(QFDDPrecondition qfddPrecondition) {
      qfddPreconditionList.add(qfddPrecondition);
      return this;
    }

    public QFDDFeedback build() {
      ModelUtil.checkNull(feedBackText, "Feedback text is mandatory");
      return new QFDDFeedback(this);
    }
  }

  public String getLanguage() {
    return language;
  }

  public String getFeedBackText() {
    return feedBackText;
  }

  public void addQFDDPrecondition(QFDDPrecondition qfddPrecondition) {
    qfddPreconditionList.add(qfddPrecondition);
  }

  public List<QFDDPrecondition> getQFDDPreconditionList() {
    return qfddPreconditionList;
  }
}
