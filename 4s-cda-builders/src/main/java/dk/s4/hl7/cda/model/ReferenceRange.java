package dk.s4.hl7.cda.model;

/**
 * Generic class representing a range
 */
public class ReferenceRange {

  private CodedValue code;
  // Low and high values could be physical values, but values only are needed for now
  private String lowValue;
  private boolean lowValueInclusive;
  private String highValue;
  private boolean highValueInclusive;

  public CodedValue getCode() {
    return code;
  }

  public String getLowValue() {
    return lowValue;
  }

  public boolean isLowValueInclusive() {
    return lowValueInclusive;
  }

  public String getHighValue() {
    return highValue;
  }

  public boolean isHighValueInclusive() {
    return highValueInclusive;
  }

  public static class ReferenceRangeBuilder {
    private CodedValue code;
    private String lowValue;
    private boolean lowValueInclusive;
    private String highValue;
    private boolean highValueInclusive;

    public ReferenceRange build() {
      return new ReferenceRange(this);
    }

    public ReferenceRangeBuilder setCode(CodedValue code) {
      this.code = code;
      return this;
    }

    public ReferenceRangeBuilder setLowValue(String lowValue, boolean inclusive) {
      this.lowValue = lowValue;
      this.lowValueInclusive = inclusive;
      return this;
    }

    public ReferenceRangeBuilder setHighValue(String highValue, boolean inclusive) {
      this.highValue = highValue;
      this.highValueInclusive = inclusive;
      return this;
    }
  }

  private ReferenceRange(ReferenceRangeBuilder builder) {
    super();
    this.code = builder.code;
    this.lowValue = builder.lowValue;
    this.lowValueInclusive = builder.lowValueInclusive;
    this.highValue = builder.highValue;
    this.highValueInclusive = builder.highValueInclusive;
  }

}
