package dk.s4.hl7.cda.convert.v11;

import java.io.IOException;

import dk.s4.hl7.cda.convert.APDXmlConverter;
import dk.s4.hl7.cda.convert.decode.v11.StatusV11Impl;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.Status;
import dk.s4.hl7.cda.model.apd.StatusV11;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

public class APDV11XmlConverter extends APDXmlConverter {

  public APDV11XmlConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    super(xmlPrettyPrinter);
  }

  protected void buildEffectiveTime(AppointmentDocument source, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("effectiveTime");
    xmlBuilder
        .element("low")
        .attribute("value", dateTimeformatter.format(source.getServiceStartTime()))
        .elementShortEnd();
    xmlBuilder
        .element("high")
        .attribute("value", dateTimeformatter.format(source.getServiceStopTime()))
        .elementShortEnd();
    xmlBuilder.elementEnd(); // end effectiveTime
  }

  protected String getStatusCode(AppointmentDocument a) {
    Status appointmentStatus = a.getAppointmentStatus();
    final StatusV11 status = StatusV11Impl.valueOf(appointmentStatus);

    switch (status) {
    case ACTIVE:
      return "active";
    case COMPLETED:
      return "completed";
    case ABORTED:
      return "aborted";
    case SUSPENDED:
      return "suspended";
    }
    throw new IllegalStateException("Invalid Appointment status" + appointmentStatus);
  }
}
