package dk.s4.hl7.cda.convert;

import java.io.Reader;

import dk.s4.hl7.cda.convert.base.AppendableSerializer;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;

/**
 * The implementation of Codec to encode and decode QFDD from objects to XML
 * and XML to objects. After construction the codec is considered thread-safe.
 * 
 * @see https://www.dartlang.org/articles/libraries/converters-and-codecs
 * 
 * @author Frank Jacobsen Systematic
 */
public class QFDDXmlCodec
    implements Codec<QFDDDocument, String>, AppendableSerializer<QFDDDocument>, ReaderSerializer<QFDDDocument> {
  private QFDDXmlConverter qfddXmlConverter;
  private XmlQFDDConverter xmlQFDDConverter;

  public QFDDXmlCodec() {
    this(new XmlPrettyPrinter());
  }

  /**
   * Create QFDDXmlCodec
   * 
   * @param xmlPrettyPrinter May be null
   */
  public QFDDXmlCodec(XmlPrettyPrinter xmlPrettyPrinter) {
    this.qfddXmlConverter = new QFDDXmlConverter(xmlPrettyPrinter);
    this.xmlQFDDConverter = new XmlQFDDConverter();
  }

  public String encode(QFDDDocument source) {
    return qfddXmlConverter.convert(source);
  }

  @Override
  public void serialize(QFDDDocument source, Appendable appendable) {
    qfddXmlConverter.serialize(source, appendable);
  }

  public QFDDDocument decode(String source) {
    return xmlQFDDConverter.convert(source);
  }

  @Override
  public QFDDDocument deserialize(Reader source) {
    return xmlQFDDConverter.deserialize(source);
  }
}
