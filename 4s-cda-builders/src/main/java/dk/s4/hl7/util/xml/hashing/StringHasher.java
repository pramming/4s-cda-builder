package dk.s4.hl7.util.xml.hashing;

public class StringHasher {
  private static final int MULTIPLIER = 31;
  private static final int MULTIPLIER_INVERSE = -1108378657;

  private int hash;

  public StringHasher() {
    this.hash = 0;
  }

  public int getHash() {
    return hash;
  }

  // See:
  // http://stackoverflow.com/questions/20808664/java-hashcode-reverse-calculation
  public StringHasher decreaseHash(int newLength, CharSequence charSequence) {
    int h = hash;
    for (int i = charSequence.length() - 1; i >= newLength; i--) {
      h = (h - charSequence.charAt(i)) * MULTIPLIER_INVERSE;
    }
    hash = h;
    return this;
  }

  public StringHasher appendHash(int currentIndex, CharSequence charSequence) {
    int h = hash;
    for (int i = currentIndex; i < charSequence.length(); i++) {
      h = MULTIPLIER * h + charSequence.charAt(i);
    }
    hash = h;
    return this;
  }

  public static int hash(CharSequence sequence) {
    return new StringHasher().appendHash(0, sequence).getHash();
  }

  public void reset() {
    hash = 0;
  }
}
