package dk.s4.hl7.util.xml;

public class RawTextHandler implements XmlHandler {
  private String xpath;
  private String elementName;
  private String rawText;
  private String defaultText;

  public RawTextHandler(String xpath, String elementName, String defaultText) {
    super();
    this.xpath = xpath;
    this.elementName = elementName;
    this.defaultText = defaultText;
    this.rawText = defaultText;
  }

  public RawTextHandler(String xpath, String elementName) {
    this(xpath, elementName, "");
  }

  public String getRawText() {
    return rawText;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase(elementName)) {
      xmlElement.startCaptureRawText();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase(elementName)) {
      rawText = xmlElement.getElementValue();
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(xpath, this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove(xpath);
  }

  public void clear() {
    rawText = defaultText;
  }
}
