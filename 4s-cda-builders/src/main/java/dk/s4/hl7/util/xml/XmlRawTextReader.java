package dk.s4.hl7.util.xml;

import java.io.IOException;
import java.io.Reader;

public class XmlRawTextReader extends Reader {
  private Reader reader;
  private Buffer buffer;

  public XmlRawTextReader(Reader reader) {
    this.reader = reader;
    this.buffer = new Buffer();
  }

  @Override
  public int read(char[] cbuf, int off, int len) throws IOException {
    int result = reader.read(cbuf, off, len);
    buffer.addToBuffer(cbuf, off, result);
    return result;
  }

  @Override
  public int read() throws IOException {
    int result = super.read();
    buffer.addToBuffer(new char[] { (char) result }, 0, result);
    return result;
  }

  @Override
  public int read(char[] cbuf) throws IOException {
    int result = super.read(cbuf);
    buffer.addToBuffer(cbuf, 0, result);
    return result;
  }

  @Override
  public void close() throws IOException {
    reader.close();
    buffer = null;
  }

  public void continuesCapture() {
    buffer.continuesCapture();
  }

  public String readRawText(int startIndex, int endIndex) {
    return buffer.readRawText(startIndex, endIndex);
  }

  private static class Buffer {
    private static final int BUILDER_BUFFER_CAPACITY = 8192;
    private StringBuilder builderBuffer;
    private boolean continueCapture;
    private int currentEndOffset;
    private int currentStartOffset;

    public Buffer() {
      this.currentEndOffset = 0;
      this.currentStartOffset = 0;
      builderBuffer = new StringBuilder(BUILDER_BUFFER_CAPACITY);
    }

    public void addToBuffer(char[] cbuf, int off, int result) {
      if (result > 0) {
        if (continueCapture) {
          appendDataToBuffer(cbuf, off, result);
        } else {
          insertDataInBuffer(cbuf, off, result);
        }
      }
    }

    private void appendDataToBuffer(char[] cbuf, int off, int result) {
      currentEndOffset += result; // Move endOffset the amount of read characters
      // Append from cbuf by offset and length
      builderBuffer.append(cbuf, off, result);
    }

    private void insertDataInBuffer(char[] cbuf, int off, int result) {
      // Move startOffset to endOffset and take current offset into account
      currentStartOffset = currentEndOffset - off;
      currentEndOffset += result; // Move endOffset the amount of read characters
      // Make complete cbuf copy
      builderBuffer.setLength(0);
      builderBuffer.append(cbuf, 0, result);
    }

    public void continuesCapture() {
      continueCapture = true;
    }

    public String readRawText(int startIndex, int endIndex) {
      continueCapture = false;
      endIndex = endIndex - currentStartOffset;
      startIndex = findProperStartIndex(startIndex - currentStartOffset);
      if (startIndex >= endIndex) {
        // Occurs with <test/> and <test></test>
        return "";
      }
      return builderBuffer.substring(startIndex, endIndex);
    }

    private int findProperStartIndex(int startIndex) {
      while (startIndex < builderBuffer.length() && builderBuffer.charAt(startIndex) != '>') {
        startIndex++;
      }
      return startIndex + 1; // We don't want the actual '>'
    }
  }
}
