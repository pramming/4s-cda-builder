package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.XMLUnit;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.testutil.HelperMethods;

public class BaseDecodeTest {

  protected <E extends ClinicalDocument> void encodeDecodeAndCompare(Codec<E, String> codec, E cda) {
    String expectedEncodedXml = null;
    E expectedDecoded = null;
    String expectedEncodedNew = null;

    try {
      expectedEncodedXml = encode(codec, cda);
      byte ptext[] = expectedEncodedXml.getBytes();
      String value = new String(ptext);
      expectedDecoded = decode(codec, value);
      expectedEncodedNew = encode(codec, expectedDecoded);
      compare(expectedEncodedXml, expectedEncodedNew);
    } catch (Exception e) {
      outXml(expectedEncodedXml, expectedEncodedNew, new StringBuffer("Kan ikke parse XML, det er ikke valid"));
      assertFalse("Kan ikke parse XML, det er ikke valid", true);
    }
  }

  protected <E extends ClinicalDocument> E decode(Codec<E, String> codec, String qrdAsXML) {
    return codec.decode(qrdAsXML);
  }

  protected <E extends ClinicalDocument> String encode(Codec<E, String> codec, E qrdDocument) {
    return codec.encode(qrdDocument);
  }

  protected void compare(String expectedXml, String computedXml) throws SAXException, IOException {
    // Use XMLDfiff for comparison, ignore differences in whitespace
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreComments(true);
    XMLUnit.setIgnoreAttributeOrder(true);
    Diff xmlDiff = new Diff(expectedXml, computedXml);
    StringBuffer assertMessage = new StringBuffer(), detailedMessage = new StringBuffer();
    xmlDiff.overrideDifferenceListener(new DebugDifferenceListener(assertMessage, detailedMessage));
    boolean similar = xmlDiff.similar();

    // For debugging, you can output all the differences!
    if (!similar) {
      outXml(expectedXml, computedXml, detailedMessage);
    }

    assertTrue(assertMessage.toString(), similar);
  }

  private void outXml(String expectedXml, String computedXml, StringBuffer detailedMessage) {
    System.out.println("------------- Detailed Message --------------");
    System.out.print(HelperMethods.indentLines(detailedMessage.toString()));
    System.out.println("------------- Expected XML ------------------");
    System.out.print(HelperMethods.indentLines(expectedXml));
    System.out.println("------------- Computed XML ------------------");
    System.out.print(HelperMethods.indentLines(computedXml));
    System.out.println("------------- End of details ----------------");
  }

  private class DebugDifferenceListener implements DifferenceListener {

    private StringBuffer assertMessage;
    private StringBuffer detailedMessage;

    DebugDifferenceListener(StringBuffer assertMessage, StringBuffer detailedMessage) {
      this.assertMessage = assertMessage;
      this.detailedMessage = detailedMessage;
    }

    @Override
    public int differenceFound(Difference diff) {
      detailedMessage.append(diff.toString()).append("\n");
      assertMessage.append(diff.getDescription()).append("\n");
      return RETURN_ACCEPT_DIFFERENCE;
    }

    @Override
    public void skippedComparison(Node node1, Node node2) {

    }
  }
}
