package dk.s4.hl7.cda.convert.encode.qrd;

import org.apache.log4j.BasicConfigurator;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.qrd.QRDDocument;

/**
 * Test QRD with no response
 */
public final class SetupQRDQuestionnaireTypeExample extends QRDExampleBase {
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  @Override
  public QRDDocument createDocument() throws Exception {
    BasicConfigurator.configure();
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    cda.setQuestionnaireType(
        new CodedValue("KCQCOP-10", "1.2.208.999.9.9", "Kansas City COPD Questionnaire", "PRO Spørgeskematyper"));
    return cda;
  }
}
