package dk.s4.hl7.cda.model.testutil;

import java.util.Calendar;

import dk.s4.hl7.cda.model.*;

/**
 * This class contains a set of static create methods that is widely reused
 * throughout all the setup code for the individual examples.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class Setup {

  public static AddressData defineNancyAddress() {
    AddressData nancyAddress = new AddressData.AddressBuilder("5700", "Svendborg")
        .setCountry("Danmark")
        .addAddressLine("Skovvejen 12")
        .addAddressLine("Landet")
        .setUse(AddressData.Use.HomeAddress)
        .build();
    return nancyAddress;
  }

  public static AddressData defineHjerteMedicinskAfdAddress() {
    AddressData hjertemedicinskAddress = new AddressData.AddressBuilder("5700", "Svendborg")
        .addAddressLine("Hjertemedicinsk afdeling B")
        .addAddressLine("Valdemarsgade 53")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    return hjertemedicinskAddress;
  }

  public static AddressData defineValdemarsGade53Address() {
    AddressData valdemarsGade53 = new AddressData.AddressBuilder("5700", "Svendborg")
        .addAddressLine("Valdemarsgade 53")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    return valdemarsGade53;
  }

  public static AddressData defineNeurologiskAfdAddress() {
    AddressData hjertemedicinskAddress = new AddressData.AddressBuilder("5700", "Svendborg")
        .addAddressLine("Neurologisk afdeling C")
        .addAddressLine("Valdemarsgade 53")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
    return hjertemedicinskAddress;
  }

  public static Patient defineNancyAsFullPersonIdentity() {
    return defineNancyAsFullPersonIdentity("2512484916");
  }

  public static Patient defineNancyAsFullPersonIdentity(String cpr) {

    Patient nancy = new Patient.PatientBuilder("Berggren")
        .setGender(Patient.Gender.Female)
        .setBirthTime(1948, Calendar.DECEMBER, 25)
        .addGivenName("Nancy")
        .addGivenName("Ann")
        .setSSN(cpr)
        .setAddress(defineNancyAddress())
        .addTelecom(AddressData.Use.HomeAddress, "tel", "65123456")
        .addTelecom(AddressData.Use.WorkPlace, "mailto", "nab@udkantsdanmark.dk")
        .build();

    return nancy;
  }

}
