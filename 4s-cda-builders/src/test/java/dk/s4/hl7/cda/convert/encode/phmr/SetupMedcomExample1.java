package dk.s4.hl7.cda.convert.encode.phmr;

import java.util.Date;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.codes.UCUM;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.DataInputContext;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

/**
 * Helper methods to create SimpleClinicalDocuments that match those of MedCom's
 * example 1.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class SetupMedcomExample1 {

  /** Define a CDA for the Medcom example 1. */
  public static PHMRDocument defineAsCDA() {
    PHMRDocument cda = defineAsCDAWitoutMedicalEquipment();

    // 1.5 Add measuring equipment
    MedicalEquipment equipment = new MedicalEquipment.MedicalEquipmentBuilder()
        .setMedicalDeviceCode("MCI00001")
        .setMedicalDeviceDisplayName("Weight")
        .setManufacturerModelName("Manufacturer: AD Company / Model: 6121ABT1")
        .setSoftwareName("SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711")
        .build();
    cda.addMedicalEquipment(equipment);

    return cda;
  }

  /**
   * Define a CDA for the Medcom example 1 but without medical equipment
   * section.
   */
  public static PHMRDocument defineAsCDAWitoutMedicalEquipment() {

    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen = new PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("aa2386d0-79ea-11e3-981f-0800200c9a66")
        .setRoot(MedCom.ROOT_OID)
        .build();
    PHMRDocument cda = new PHMRDocument(idHeader);
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    cda.setPatient(nancy);
    cda.setTitle("Hjemmemonitorering for " + nancy.getIdValue());
    cda.setLanguageCode("da-DK");

    // 1.1 Populate with time and version info
    cda.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("241301000016007")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    OrganizationIdentity svendborgHjerteMedicinskAfdeling = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("241301000016007")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65112233")
        .build();

    cda.setAuthor(new ParticipantBuilder()
        .setSOR("241301000016007")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65112233")
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .setTime(documentCreationTime)
        .build());

    cda.setCustodian(svendborgHjerteMedicinskAfdeling);
    Date at1000onJan13 = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);
    cda.setLegalAuthenticator(new ParticipantBuilder()
        .setSOR("241301000016007")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65112233")
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .setTime(at1000onJan13)
        .build());

    // 1.4 Define the service period
    Date from = DateUtil.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = DateUtil.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    cda.setDocumentationTimeInterval(from, to);

    ID id = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("aa2386d0-79ea-11e3-981f-0800200c9a66")
        .setRoot(MedCom.ROOT_OID)
        .build();

    // 1.6 Add measurements (observations)

    // Example 1: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.
    Date time1 = DateUtil.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.Electronically,
        DataInputContext.PerformerType.Citizen);

    Measurement weight1 = NPU.createWeight("77.5", time1, context, id);
    cda.addResult(weight1);

    // Use the basic methods that allow any legal
    // code system to be used but requires all data to be
    // provided
    Date time2 = DateUtil.makeDanishDateTime(2014, 0, 8, 7, 45, 0);
    Measurement weight2 = new Measurement.MeasurementBuilder(time2, Measurement.Status.COMPLETED)
        .setPhysicalQuantity("77.0", UCUM.kg, NPU.DRY_BODY_WEIGHT_CODE, NPU.DRY_BODY_WEIGHT_DISPLAYNAME)
        .setContext(context)
        .setId(id)
        .build();
    cda.addResult(weight2);

    Date time3 = DateUtil.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    Measurement weight3 = new Measurement.MeasurementBuilder(time3, Measurement.Status.COMPLETED)
        .setContext(context)
        .setPhysicalQuantity("77.2", UCUM.kg, NPU.DRY_BODY_WEIGHT_CODE, NPU.DRY_BODY_WEIGHT_DISPLAYNAME)
        .setId(id)
        .build();

    CodedValue codeValue = new CodedValue("q4", "2.16.840.1.113883.6.1", "Some-Display-Name", "Some-CodeSystem-Name");

    // External reference
    ID idDocRef = new ID.IDBuilder().setRoot(MedCom.ROOT_OID).setExtension("externalDocumentReference").build();
    Reference reference1 = new Reference.ReferenceBuilder(
        Reference.DocumentIdReferencesUse.XDS_UNIQUE_ID_REFERENCE.getReferencesUse(), idDocRef, codeValue).build();
    // External reference with observation
    ID idDocObsRef = new ID.IDBuilder().setRoot(MedCom.MEDCOM_PROMPT_OID).setExtension("externalObservation").build();
    Reference reference2 = new Reference.ReferenceBuilder(
        Reference.DocumentIdReferencesUse.XDS_UNIQUE_ID_REFERENCE.getReferencesUse(), idDocRef, codeValue)
            .externalObservation(idDocObsRef)
            .build();

    weight3.addReference(reference1);
    weight3.addReference(reference2);

    cda.addResult(weight3);

    return cda;
  }
}
