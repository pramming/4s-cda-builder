package dk.s4.hl7.cda.model.testutil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.hl7.v3.POCDMT000040ClinicalDocument;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Various methods to help in the testing.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public class HelperMethods {
  private static volatile JAXBContext jaxbContext;
  private static volatile Object lock = new Object();

  private static JAXBContext getOrCreateJAXbContext() {
    try {
      if (jaxbContext == null) {
        synchronized (lock) {
          if (jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(POCDMT000040ClinicalDocument.class);
          }
        }
      }
      return jaxbContext;
    } catch (JAXBException e1) {
      throw new RuntimeException(e1.getMessage(), e1);
    }
  }

  /**
   * convert an XML document to a human readable string with proper indentation.
   * 
   * @param doc
   *          the XML document to convert
   * @return the string representation of the document.
   */
  private static TransformerFactory transfac = TransformerFactory.newInstance();

  public static String convertXMLDocumentToString(Node doc) {

    Transformer trans = null;
    try {
      trans = transfac.newTransformer();
    } catch (TransformerException e) {
      System.err.println(e);
    }
    // trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    trans.setOutputProperty(OutputKeys.INDENT, "yes");
    trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

    // create string from xml tree
    StringWriter sw = new StringWriter();
    StreamResult result = new StreamResult(sw);
    DOMSource source = new DOMSource(doc);
    try {
      trans.transform(source, result);
    } catch (TransformerException e) {
      System.err.println(e);
    }
    // sw.close();

    String xmlString = sw.toString();

    return xmlString;
  }

  /**
   * get the value of a specific attribute with an enclosing node. Example:
   *
   * The PHMR contains a deeply nested observation node like this {@code 
   *   <observation classCode="OBS" moodCode="EVN"> 
   *     <templateId root="2.16.840.1.113883.10.20.1.31"/> 
   *     <templateId root="2.16.840.1.113883.10.20.9.8"/> 
   *     
  <code code="20150-9" codeSystem="2.16.840.1.113883.6.1" displayName="FEV1"/> 
   *     <value unit="L" value="3.42" xsi:type="PQ"/> 
   *   < /observation>
   * } The following test will pass: assertEquals("L",
   * getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("unit", 1, "value",
   * "observation", phmrDoc));
   *
   * @param attributeName
   *          name of the attribute whose value is returned
   * @param nodeIndex
   *          the number of the node named 'nodeName' in the child list of node
   *          'enclosingNodeName'
   * @param nodeName
   *          name of the node with the attribute
   * @param enclosingNodeName
   *          name of the node that encloses the node
   * @param doc
   *          the XML document
   * @return the value of the attribute or null if not found.
   */
  public static String getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc(String attributeName, int nodeIndex,
      String nodeName, String enclosingNodeName, Document doc) {
    NodeList list = doc.getElementsByTagName(enclosingNodeName);
    NodeList childrenOfEnclosed = list.item(nodeIndex).getChildNodes();
    for (int j = 0; j < childrenOfEnclosed.getLength(); j++) {
      if (childrenOfEnclosed.item(j).getNodeName().equals(nodeName)) {
        NamedNodeMap nnm = childrenOfEnclosed.item(j).getAttributes();
        return nnm.getNamedItem(attributeName).getNodeValue();
      }
    }
    return null;
  }

  /**
   * Indent all lines in text.
   * 
   * @param text
   *          to indent.
   * @return indented text.
   */
  public static String indentLines(String text) {
    final String indent = "  ";
    return indent + text.replaceAll("\n", "\n" + indent);
  }

  public static Document parseXMLStringToDOM(String xmltext, boolean forceNamespace)
      throws ParserConfigurationException, SAXException, IOException, JAXBException {
    return parseXMLStringToDOM(new StringReader(xmltext), forceNamespace);
  }

  public static Document parseXMLStringToDOM(Reader reader, boolean forceNamespace)
      throws ParserConfigurationException, SAXException, IOException, JAXBException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setNamespaceAware(forceNamespace);
    Document dom = factory.newDocumentBuilder().parse(new InputSource(reader));
    testPOCDMT000040ClinicalDocumentTrue(dom); // Is valid CDA XML
    return dom;
  }

  public static Document parseXMLStringToDOM(File file, boolean forceNamespace)
      throws ParserConfigurationException, SAXException, IOException, JAXBException {
    if (!file.exists()) {
      throw new RuntimeException("Input file does not exists: " + file.getAbsolutePath());
    }
    if (!file.isFile()) {
      throw new RuntimeException("Input file is not a file: " + file.getAbsolutePath());
    }
    Reader reader = FileUtil.getReader(file, Charset.forName("UTF-8"));
    try {
      return parseXMLStringToDOM(reader, forceNamespace);
    } finally {
      if (reader != null) {
        reader.close();
      }
    }
  }

  public static void addStyleSheetHeader(Document doc) {
    ProcessingInstruction instruction = doc.createProcessingInstruction("xml-stylesheet",
        "type=\"text/xsl\" href=\"../Stylesheet/cda.xsl\"");
    doc.insertBefore(instruction, doc.getDocumentElement());
  }

  public static void testPOCDMT000040ClinicalDocumentTrue(Document asString) throws JAXBException {
    try {
      Unmarshaller unmarshaller = getOrCreateJAXbContext().createUnmarshaller();
      unmarshaller.unmarshal(asString);
    } catch (JAXBException e) {
      throw e;
    }
  }
}
