package dk.s4.hl7.cda.examples.medcom;

import java.util.Date;
import java.util.UUID;

import org.junit.Test;

import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public class QRDLungInformationNeedsQuestionaire {
  @Test
  public void createQrd() {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as author
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a QFDD document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID id = new ID.IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension(uuid())
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    QRDDocument document = new QRDDocument(id);
    document.setTitle("Spørgeskema til patienter med kronisk lungesygdom");

    String qfddReferenceUuid = "5f971389-13cd-42b6-bef1-fcd46543e93a";

    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    document.setPatient(nancy);

    // 1.1 Populate with time and version info
    document.setEffectiveTime(documentCreationTime);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    document.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    document.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Date at1000onJan13 = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    document.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(at1000onJan13)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Section<QRDResponse> section = new Section<QRDResponse>("Spørgeskema", "Spørgeskema");

    section.addQuestionnaireEntity(
        createMultipleChoiceResponse("1", "Kender du navnet på din lungesygdom?", "A1", "ja", qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("2",
        "Har en læge eller sygeplejerske fortalt dig, hvordan denne sygdom påvirker dine lunger?", "A1", "ja",
        qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("3",
        "Har en læge eller sygeplejerske fortalt dig, hvad der sandsynligvis vil ske i fremtiden?", "A1", "ja",
        qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("4",
        "Hvilke af de følgende udsagn beskriver bedst, hvad der vil ske med dig i løbet af de nærmeste år?", "A1",
        "Nu, da min sygdom bliver behandlet, får jeg det nok bedre", qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("5",
        "Har en læge eller sygeplejerske forklaret dig grunden til, at du skal bruge inhalator eller tage medicin?",
        "A1", "ja", qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("6",
        "Bestræber du dig på at bruge din inhalator eller tage din medicin, nøjagtigt som en læge eller sygeplejerske har lært dig?",
        "A2", "nej", qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("7",
        "Er du tilfreds med den information, læger og sygeplejersker har givet dig om dine inhalatorer og din medicin?",
        "A1", "Jeg føler mig velinformeret om det, jeg skal vide", qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("8",
        "Hvilket udsagn beskriver bedst, hvad du har fået at vide, at du skal gøre, hvis din vejrtrækning bliver mere besværet (fx tage to pust i stedet for et)?",
        "A1", "Jeg har fået at vide, hvad jeg skal gøre, og lægen/sygeplejersken har givet mig en skriftlig vejledning",
        qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("9",
        "Har du fået at vide, hvornår du skal ringe efter en ambulance, hvis din vejrtrækning bliver mere besværet?",
        "A1", "Jeg har fået at vide, hvad jeg skal gøre, og lægen/sygeplejersken har givet mig en skriftlig vejledning",
        qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("10", "Hvilket udsagn beskriver dig bedst?",
        "Aldrig røget (gå til spørgsmål 13)", "A1", qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("13",
        "Har en læge eller sygeplejerske fortalt dig, at du skal forsøge at dyrke motion (fx almindelig gang, rask gang og andre former for motion)?",
        "A2", "nej", qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("14",
        "Har en læge eller sygeplejerske fortalt dig, hvor meget motion (fx almindelig gang, rask gang og andre former for motion), du skal dyrke?",
        "A1", "Ja, og jeg ved, hvad jeg skal gøre", qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("15", "Hvor meget motion dyrker du?", "A1",
        "Så lidt som muligt", qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("16",
        "Hvad har læger eller sygeplejersker fortalt dig om dine kost- og spisevaner? (Sæt kryds for hvert svar der passer for dig)",
        4, "A1", "Ingenting", qfddReferenceUuid));
    section.addQuestionnaireEntity(createTextResponse("17",
        "Har du spørgsmål eller kommentarer ang. din lungesygdom? Hvis du har så skriv dem herunder:",
        "Det har jeg ikke", qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("18", "Bor du alene?", "A2", "nej", qfddReferenceUuid));
    section.addQuestionnaireEntity(
        createMultipleChoiceResponse("19", "Hvad er dit køn?", "A2", "kvinde", qfddReferenceUuid));
    section.addQuestionnaireEntity(createTextResponse("20", "Hvilket år er du født?", "1975", qfddReferenceUuid));

    document.addSection(section);
  }

  private QRDTextResponse createTextResponse(String questionCode, String question, String answer,
      String qfddReference) {
    CodedValue codeValue = new CodedValue(questionCode, MedCom.MEDCOM_PROMPT_OID, "", MedCom.MEDCOM_PROMPT_TABLE);
    ID id = new ID.IDBuilder()
        .setRoot(MedCom.MEDCOM_PROMPT_OID)
        .setExtension(uuid())
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    QRDTextResponse response = new QRDTextResponse.QRDTextResponseBuilder()
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion(question)
        .setText(answer)
        .build();

    ID qfddId = new ID.IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension(qfddReference)
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    CodedValue qfddCodedValue = new CodedValueBuilder()
        .setCode(Loinc.QFD_CODE)
        .setCodeSystem(Loinc.OID)
        .setDisplayName(Loinc.DISPLAYNAME)
        .build();
    response.addReference(
        new Reference.ReferenceBuilder(Reference.DocumentIdReferencesUse.CDA_DOCUMENT_ID_REFERENCE.getReferencesUse(),
            qfddId, qfddCodedValue).build());
    return response;
  }

  private QRDMultipleChoiceResponse createMultipleChoiceResponse(String questionCode, String question, int maximum,
      String answerId, String answerText, String qfddReference) {
    CodedValue codeValue = new CodedValue(questionCode, MedCom.MEDCOM_PROMPT_OID, "", MedCom.MEDCOM_PROMPT_TABLE);
    ID id = new ID.IDBuilder()
        .setRoot(MedCom.MEDCOM_PROMPT_OID)
        .setExtension(uuid())
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    QRDMultipleChoiceResponse response = new QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder()
        .setInterval(1, maximum)
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion(question)
        .build();

    response.addAnswer(answerId, MedCom.MEDCOM_PROMPT_OID, answerText, MedCom.MEDCOM_PROMPT_TABLE);
    ID qfddId = new ID.IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension(qfddReference)
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    CodedValue qfddCodedValue = new CodedValueBuilder()
        .setCode(Loinc.QFD_CODE)
        .setCodeSystem(Loinc.OID)
        .setDisplayName(Loinc.DISPLAYNAME)
        .build();
    response.addReference(
        new Reference.ReferenceBuilder(Reference.DocumentIdReferencesUse.CDA_DOCUMENT_ID_REFERENCE.getReferencesUse(),
            qfddId, qfddCodedValue).build());
    return response;
  }

  private QRDMultipleChoiceResponse createMultipleChoiceResponse(String questionCode, String question, String answerId,
      String answerText, String qfddReference) {
    return createMultipleChoiceResponse(questionCode, question, 1, answerId, answerText, qfddReference);
  }

  private String uuid() {
    return UUID.randomUUID().toString();
  }
}
