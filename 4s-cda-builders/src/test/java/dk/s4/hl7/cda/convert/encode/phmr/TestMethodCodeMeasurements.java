package dk.s4.hl7.cda.convert.encode.phmr;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.model.DataInputContext;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.cda.model.util.DateUtil;

public final class TestMethodCodeMeasurements {
  private static PHMRDocument cda;

  @BeforeClass
  public static void setup() {
    // Step 1. Define the Medcom EX1 CDA
    cda = SetupMedcomExample1.defineAsCDA();
  }

  @Test
  public void shouldValidateMedComCodesForEx1() throws Exception {
    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.Electronically,
        DataInputContext.PerformerType.Citizen);

    ID id = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("021ba3bd-6935-45ca-bdcd-ed77f8b2ee2a")
        .setRoot(MedCom.ROOT_OID)
        .build();

    String asString = buildXMLStringForCDAWithContextMeasurement(context, id);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"POT\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("displayName=\"Målt af borger\" codeSystemName=\"MedCom Message Codes\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"AUT\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("displayName=\"Måling overført automatisk\" codeSystemName=\"MedCom Message Codes\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesByHealthcareProfessionals() throws Exception {
    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.TypedByHealthcareProfessional,
        DataInputContext.PerformerType.HealthcareProfessional);

    ID id = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("021ba3bd-6935-45ca-bdcd-ed77f8b2ee2a")
        .setRoot(MedCom.ROOT_OID)
        .build();

    String asString = buildXMLStringForCDAWithContextMeasurement(context, id);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"PNT\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("displayName=\"Målt af aut. sundhedsperson\" codeSystemName=\"MedCom Message Codes\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPH\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong", asString
        .contains("displayName=\"Indtastet af aut. sundhedsperson\" codeSystemName=\"MedCom Message Codes\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesForCaregiver() throws Exception {
    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.TypedByCareGiver,
        DataInputContext.PerformerType.CareGiver);

    ID id = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("021ba3bd-6935-45ca-bdcd-ed77f8b2ee2a")
        .setRoot(MedCom.ROOT_OID)
        .build();

    String asString = buildXMLStringForCDAWithContextMeasurement(context, id);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"PCG\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("displayName=\"Målt af anden omsorgsperson\" codeSystemName=\"MedCom Message Codes\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPC\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong", asString
        .contains("displayName=\"Indtastet af anden omsorgsperson\" codeSystemName=\"MedCom Message Codes\"/>"));
  }

  @Test
  public void shouldValidateMedComCodesForRelative() throws Exception {
    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.TypedByCitizenRelative,
        DataInputContext.PerformerType.Citizen);

    ID id = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("021ba3bd-6935-45ca-bdcd-ed77f8b2ee2a")
        .setRoot(MedCom.ROOT_OID)
        .build();

    String asString = buildXMLStringForCDAWithContextMeasurement(context, id);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"POT\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("displayName=\"Målt af borger\" codeSystemName=\"MedCom Message Codes\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPR\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("displayName=\"Indtastet af pårørende\" codeSystemName=\"MedCom Message Codes\"/>"));

  }

  @Test
  public void shouldValidateMedComCodesForCitizen() throws Exception {
    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.TypedByCitizen,
        DataInputContext.PerformerType.Citizen);

    ID id = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("021ba3bd-6935-45ca-bdcd-ed77f8b2ee2a")
        .setRoot(MedCom.ROOT_OID)
        .build();

    String asString = buildXMLStringForCDAWithContextMeasurement(context, id);

    assertTrue("methodCode (Measurer 1) missing or wrong",
        asString.contains("<methodCode code=\"POT\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (Measurer 2) missing or wrong",
        asString.contains("displayName=\"Målt af borger\" codeSystemName=\"MedCom Message Codes\"/>"));

    assertTrue("methodCode (ProvisionMethod 1) missing or wrong",
        asString.contains("<methodCode code=\"TPD\" codeSystem=\"1.2.208.184.100.1\""));
    assertTrue("methodCode (ProvisionMethod 2) missing or wrong",
        asString.contains("displayName=\"Indtastet af borger\" codeSystemName=\"MedCom Message Codes\"/>"));
  }

  private String buildXMLStringForCDAWithContextMeasurement(DataInputContext context, ID id) {
    Date when = DateUtil.makeDanishDateTime(2014, 0, 14, 9, 45, 00);
    cda.addResult(new Measurement.MeasurementBuilder(when, Measurement.Status.COMPLETED)
        .setPhysicalQuantity("138.0", "mm[Hg]", "DNK05472", "Blodtryk systolisk;Arm")
        .setContext(context)
        .setId(id)
        .build());
    return buildXMLStringForCDA(cda);
  }

  private String buildXMLStringForCDA(PHMRDocument cda) {
    // 2. Convert it into a Danish PHMR XML format
    PHMRXmlCodec phmrCodec = new PHMRXmlCodec();

    // 3. Extract the acutal XML DOM and string representation
    return phmrCodec.encode(cda);
  }
}
