package dk.s4.hl7.cda.convert.encode.qrd;

import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Reference.DocumentIdReferencesUse;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDResponse;

/**
 * Test DiscreteSlider Example
 *
 * @author Frank Jacobsen, Systematic
 * 
 */
public final class SetupQRDDiscreteSliderExample extends QRDExampleBase {
  private static final String QUESTION = "question";
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  private QRDResponse responseWithReference() {
    return new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setMinimum(1)
        .addReference(simpleDocument())
        .setAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .build();
  }

  private Reference simpleDocument() {
    return new ReferenceBuilder(DocumentIdReferencesUse.CDA_DOCUMENT_ID_REFERENCE.getReferencesUse(),
        MedCom.createId("referenceId"), Loinc.createPHMRTypeCode()).build();
  }

  protected QRDResponse optionalResponseWithAnswer() {
    return new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "2", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setMinimum(0)
        .setAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .build();
  }

  protected QRDResponse optionalResponseNoAnswer() {
    return new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "3", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setMinimum(0)
        .build();
  }

  public QRDDocument createDocument() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    Section<QRDResponse> section = new Section<QRDResponse>("Questions", "Questions");
    section.addQuestionnaireEntity(optionalResponseWithAnswer());
    section.addQuestionnaireEntity(optionalResponseNoAnswer());
    section.addQuestionnaireEntity(responseWithReference());
    cda.addSection(section);
    return cda;
  }
}
