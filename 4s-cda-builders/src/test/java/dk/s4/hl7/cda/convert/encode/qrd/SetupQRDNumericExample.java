package dk.s4.hl7.cda.convert.encode.qrd;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Reference.DocumentIdReferencesUse;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse.QRDNumericResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDResponse;

/**
 * Test QRD numeric example
 *
 * @author Frank Jacobsen, Systematic
 * 
 */
public final class SetupQRDNumericExample extends QRDExampleBase {
  private static final String QUESTION = "question";
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  private QRDResponse responseWithReference() {
    return new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setInterval("1,00", "2,00", IntervalType.IVL_REAL)
        .setValue("1.55", BasicType.REAL)
        .addReference(simpleDocument())
        .build();
  }

  private Reference simpleDocument() {
    return new ReferenceBuilder(DocumentIdReferencesUse.CDA_DOCUMENT_ID_REFERENCE.getReferencesUse(),
        MedCom.createId("referenceId"), Loinc.createPHMRTypeCode()).build();
  }

  protected QRDResponse optionalIntervalWithInterval() {
    return new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "2", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setInterval("1,00", "2,00", IntervalType.IVL_REAL)
        .setValue("1.60", BasicType.REAL)
        .build();
  }

  protected QRDResponse optionalIntervalNoInterval() {
    return new QRDNumericResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "3", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setValue("1.70", BasicType.REAL)
        .build();
  }

  @Override
  public QRDDocument createDocument() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    Section<QRDResponse> section = new Section<QRDResponse>("Questions", "Questions");
    section.addQuestionnaireEntity(responseWithReference());
    section.addQuestionnaireEntity(optionalIntervalWithInterval());
    section.addQuestionnaireEntity(optionalIntervalNoInterval());
    cda.addSection(section);
    return cda;
  }
}
