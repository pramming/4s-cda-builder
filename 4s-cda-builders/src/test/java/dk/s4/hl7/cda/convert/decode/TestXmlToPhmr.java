package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.convert.encode.phmr.SetupMedcomExample1;
import dk.s4.hl7.cda.convert.encode.phmr.SetupMedcomKOLExample1;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.FileUtil;

public final class TestXmlToPhmr extends BaseDecodeTest implements ConcurrencyTestCase {
  private PHMRXmlCodec codec = new PHMRXmlCodec();

  @Before
  public void before() {
    setCodec(new PHMRXmlCodec());
  }

  public void setCodec(PHMRXmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testPHMRXmlConverterMedcomExample1() throws Exception {
    encodeDecodeAndCompare(codec, SetupMedcomExample1.defineAsCDA());
  }

  @Test
  public void testPHMRXmlConvertermedcomKOLExample1() {
    encodeDecodeAndCompare(codec, SetupMedcomKOLExample1.defineAsCDA());
  }

  @Test
  public void testPHMRXMLConverterOldNancy() {
    try {
      String phmrAsXML = FileUtil.getData(this.getClass(), "phmr/PhmrMeasurementVariations.xml");
      decode(codec, phmrAsXML);
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testPHMRXMLConverterMeasurementVariations() {
    try {
      String phmrAsXML = FileUtil.getData(this.getClass(), "phmr/PhmrMeasurementVariations.xml");

      PHMRDocument phmr = decode(codec, phmrAsXML);
      assertTrue(phmr.getVitalSigns().size() > 0);
      Iterator<Measurement> iterator = phmr.getVitalSigns().iterator();
      int countBySingleValue, countByLowOpen, countByHighOpen;
      countBySingleValue = countByLowOpen = countByHighOpen = 0;
      while (iterator.hasNext()) {
        Measurement m = iterator.next();
        assertTrue(m.getValue() != null || m.getValueInterval() != null);
        if (m.getValue() != null) {
          countBySingleValue++;
        } else if (m.getValueInterval() != null && m.getValueInterval().isHighValueUnknown()) {
          countByHighOpen++;
        } else if (m.getValueInterval() != null && m.getValueInterval().isLowValueUnknown()) {
          countByLowOpen++;
        } else {
          fail("Unexpected measurement");
        }
      }
      assertEquals(4, countBySingleValue);
      assertEquals(1, countByLowOpen);
      assertEquals(1, countByHighOpen);
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Override
  public void runTest() throws Exception {
    testPHMRXmlConvertermedcomKOLExample1();
  }
}
