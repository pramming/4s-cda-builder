package dk.s4.hl7.cda.convert.encode.qrd;

import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Reference.DocumentIdReferencesUse;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDResponse;

/**
 * Test MultibleChoise response.
 *
 * @author Frank Jacobsen, Systematic
 * 
 */
public final class SetupQRDMultipleChoiceExample extends QRDExampleBase {
  private static final String QUESTION = "question";
  private static final String DOCUMENT_ID = "951d558e-8775-4bb5-8db4-bfa4133ea605";

  private QRDResponse responseWithReference() {
    return new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setInterval(2, 5)
        .addAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .addAnswer("answerCode2", "answerCodeSystem2", "answerDisplayName2", "answerCodeSystemName2")
        .addAnswer("answerCode3", "answerCodeSystem3", "answerDisplayName3", "answerCodeSystemName3")
        .addReference(simpleDocument())
        .build();
  }

  private Reference simpleDocument() {
    return new ReferenceBuilder(DocumentIdReferencesUse.CDA_DOCUMENT_ID_REFERENCE.getReferencesUse(),
        MedCom.createId("referenceId"), Loinc.createPHMRTypeCode()).build();
  }

  protected QRDResponse optionalResponseWithAnswer() {
    return new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setInterval(2, 5)
        .addAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .addAnswer("answerCode2", "answerCodeSystem2", "answerDisplayName2", "answerCodeSystemName2")
        .addAnswer("answerCode3", "answerCodeSystem3", "answerDisplayName3", "answerCodeSystemName3")
        .build();
  }

  protected QRDResponse optionalResponseNoAnswer() {
    return new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code", "codeSystem", QUESTION + "1", "CodeSystemName"))
        .setId(MedCom.createId("simpleId"))
        .setQuestion(QUESTION)
        .setInterval(0, 5)
        .build();
  }

  @Override
  public QRDDocument createDocument() throws Exception {
    QRDDocument cda = createBaseQRDDocument(DOCUMENT_ID);
    Section<QRDResponse> section = new Section<QRDResponse>("Questions", "Questions");
    section.addQuestionnaireEntity(optionalResponseWithAnswer());
    section.addQuestionnaireEntity(optionalResponseNoAnswer());
    section.addQuestionnaireEntity(responseWithReference());
    cda.addSection(section);
    return cda;
  }
}
