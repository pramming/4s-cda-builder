package dk.s4.hl7.util;

import java.io.InputStream;
import java.io.StringReader;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XmlElementFinder;
import dk.s4.hl7.util.xml.XmlMapping;

public class XmlElementFinderTest {
  private static final String INNER_HTML_TEXT = "1. Heart failure affects different people in different ways. Some feel shortness of breath while others feel fatigue. Please<br/>indicate how much you are limited by heart failure (shortness of breath or fatigue) in your ability to do the following<br/>activities over the past 2 weeks.";

  @Test
  public void testHTMLAsInnerText() throws Exception {
    String inputText = "<root><text>" + INNER_HTML_TEXT + "</text><stop></stop></root>";
    XmlElementFinder finder = new XmlElementFinder();
    XmlMapping xmlMapping = new XmlMapping();
    RawTextHandler rawTextHandler = new RawTextHandler("/root/text", "text");
    rawTextHandler.addHandlerToMap(xmlMapping);
    finder.findElements(new StringReader(inputText), xmlMapping);
    Assert.assertEquals(INNER_HTML_TEXT, rawTextHandler.getRawText());
  }

  @Test
  public void testEmptyInnerText() throws Exception {
    String inputText = "<root></root>";
    XmlElementFinder finder = new XmlElementFinder();
    XmlMapping xmlMapping = new XmlMapping();
    RawTextHandler rawTextHandler = new RawTextHandler("/root", "root");
    rawTextHandler.addHandlerToMap(xmlMapping);
    finder.findElements(new StringReader(inputText), xmlMapping);
    Assert.assertEquals("", rawTextHandler.getRawText());
  }

  @Test
  public void testShortElementText() throws Exception {
    String inputText = "<root/>";
    XmlElementFinder finder = new XmlElementFinder();
    XmlMapping xmlMapping = new XmlMapping();
    RawTextHandler rawTextHandler = new RawTextHandler("/root", "root");
    rawTextHandler.addHandlerToMap(xmlMapping);
    finder.findElements(new StringReader(inputText), xmlMapping);
    Assert.assertEquals("", rawTextHandler.getRawText());
  }

  @Test
  @Ignore
  public void testXmlParserPerf() throws Exception {
    for (int i = 0; i < 20; i++) {
      InputStream is = this.getClass().getResourceAsStream("/phmr/PHMR_KOL_Example_1_MaTIS-MODIFIED.xml");
      XMLEventReader reader = XMLInputFactory.newFactory().createXMLEventReader(is);
      long start = System.currentTimeMillis();
      while (reader.hasNext()) {
        reader.nextEvent();
      }
      long end = System.currentTimeMillis();
      // TODO: What are we testing here? Think we need to decide on some values
      // to check result against!
      System.out.println("Duration: " + (end - start));
      is.close();
    }
  }
}
