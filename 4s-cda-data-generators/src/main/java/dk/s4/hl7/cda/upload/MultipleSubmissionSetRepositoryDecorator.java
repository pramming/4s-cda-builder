package dk.s4.hl7.cda.upload;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.net4care.xdsconnector.IRepositoryConnector;
import org.net4care.xdsconnector.SendRequestException;
import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.Utilities.PrepareRequestException;
import org.net4care.xdsconnector.Utilities.RetrieveDocumentSetRequestTypeBuilder;
import org.net4care.xdsconnector.service.RegistryResponseType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlStreamWrapper;

public class MultipleSubmissionSetRepositoryDecorator implements IRepositoryConnector {
  private static final Logger logger = LoggerFactory.getLogger(MultipleSubmissionSetRepositoryDecorator.class);
  private IRepositoryConnector nextRepositoryConnector;
  private XMLInputFactory xmlInputFactory;

  public MultipleSubmissionSetRepositoryDecorator(IRepositoryConnector nextRepositoryConnector) {
    this.nextRepositoryConnector = nextRepositoryConnector;
    this.xmlInputFactory = XMLInputFactory.newFactory();
  }

  @Override
  public RetrieveDocumentSetResponseType retrieveDocumentSet(RetrieveDocumentSetRequestTypeBuilder builder)
      throws SendRequestException {
    return nextRepositoryConnector.retrieveDocumentSet(builder);
  }

  @Override
  public RetrieveDocumentSetResponseType retrieveDocumentSet(String docId) throws SendRequestException {
    return nextRepositoryConnector.retrieveDocumentSet(docId);
  }

  @Override
  public RetrieveDocumentSetResponseType retrieveDocumentSet(List<String> docIds) throws SendRequestException {
    return nextRepositoryConnector.retrieveDocumentSet(docIds);
  }

  @Override
  public RegistryResponseType provideAndRegisterCDADocument(Document cda, CodedValue healthcareFacilityType,
      CodedValue practiceSettingsCode) throws PrepareRequestException, SendRequestException {
    return nextRepositoryConnector.provideAndRegisterCDADocument(cda, healthcareFacilityType, practiceSettingsCode);
  }

  @Override
  public RegistryResponseType provideAndRegisterCDADocument(String cda, CodedValue healthcareFacilityType,
      CodedValue practiceSettingsCode) throws PrepareRequestException, SendRequestException {
    return nextRepositoryConnector.provideAndRegisterCDADocument(cda, healthcareFacilityType, practiceSettingsCode);
  }

  @Override
  public RegistryResponseType provideAndRegisterCDADocuments(List<String> cdas, CodedValue healthcareFacilityType,
      CodedValue practiceSettingsCode) throws PrepareRequestException, SendRequestException {
    Map<String, List<String>> mapPatientAndTypeToDocuments = sortByPatientIdAndContentType(cdas);
    RegistryResponseType response = new RegistryResponseType();
    for (List<String> cdaDocuments : mapPatientAndTypeToDocuments.values()) {
      response = nextRepositoryConnector.provideAndRegisterCDADocuments(cdaDocuments, healthcareFacilityType,
          practiceSettingsCode);
      if (!"urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success".equalsIgnoreCase(response.getStatus())) {
        return response;
      }
    }
    return response;
  }

  public Map<String, List<String>> sortByPatientIdAndContentType(List<String> cdaDocuments) {
    Map<String, List<String>> sortedMap = new HashMap<String, List<String>>();
    for (String cdaDocument : cdaDocuments) {
      String patientAndDocumentType = findPatientAndDocumentType(cdaDocument);
      if (!patientAndDocumentType.isEmpty()) {
        mapPatientAndTypeToCdaDocument(sortedMap, patientAndDocumentType, cdaDocument);
      }
    }
    return sortedMap;
  }

  private void mapPatientAndTypeToCdaDocument(Map<String, List<String>> sortedMap, String patientAndDocumentType,
      String cdaDocument) {
    List<String> cdaDocuments = sortedMap.get(patientAndDocumentType);
    if (cdaDocuments == null) {
      cdaDocuments = new ArrayList<String>();
      cdaDocuments.add(cdaDocument);
      sortedMap.put(patientAndDocumentType, cdaDocuments);
    } else {
      cdaDocuments.add(cdaDocument);
    }
  }

  private String findPatientAndDocumentType(String cdaDocument) {
    String patient = "";
    String contentType = "";
    XmlStreamWrapper wrapper = null;
    try {
      XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(new StringReader(cdaDocument));
      wrapper = new XmlStreamWrapper(reader);
      XMLElement xmlElement = new XMLElement(wrapper);
      while (wrapper.hasNext()) {
        wrapper.next();
        if (wrapper.isCurrentStartElement()) {
          if (wrapper.getCurrentXPath().equalsIgnoreCase("/ClinicalDocument/recordTarget/patientRole/id")) {
            patient = xmlElement.getAttributeValue("extension");
            if (isFinished(patient, contentType)) {
              return patient + contentType;
            }
          } else if (wrapper.getCurrentXPath().equalsIgnoreCase("/ClinicalDocument/code")) {
            contentType = xmlElement.getAttributeValue("code");
            if (isFinished(patient, contentType)) {
              return patient + contentType;
            }
          }
        }
      }
    } catch (Exception e) {
      logger.error("Error in input CDA file: ", e);
      return "";
    } finally {
      wrapper.close();
    }
    return "";
  }

  private boolean isFinished(String patient, String contentType) {
    return !patient.isEmpty() && !contentType.isEmpty();
  }

}
