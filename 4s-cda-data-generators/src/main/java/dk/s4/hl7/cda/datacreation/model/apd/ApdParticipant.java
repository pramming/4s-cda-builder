package dk.s4.hl7.cda.datacreation.model.apd;

public class ApdParticipant extends ApdCsvObject {
  private String participantId;
  private ApdAddress address;
  private String phoneNumbers;
  private String emails;
  private ApdPerson person;
  private String organizationName;

  public String getParticipantId() {
    return participantId;
  }

  public void setParticipantId(String participantId) {
    this.participantId = participantId;
  }

  public ApdAddress getAddress() {
    return address;
  }

  public void setAddress(ApdAddress address) {
    this.address = address;
  }

  public String getPhoneNumbers() {
    return phoneNumbers;
  }

  public void setPhoneNumbers(String phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
  }

  public String getEmails() {
    return emails;
  }

  public void setEmails(String emails) {
    this.emails = emails;
  }

  public ApdPerson getPerson() {
    return person;
  }

  public void setPerson(ApdPerson person) {
    this.person = person;
  }

  public String getOrganizationName() {
    return organizationName;
  }

  public void setOrganizationName(String organizationName) {
    this.organizationName = organizationName;
  }

  @Override
  public boolean isObjectEmpty() {
    return isNullOrEmpty(this.participantId) && isNullOrEmpty(this.phoneNumbers) && isNullOrEmpty(this.emails)
        && this.person.isObjectEmpty() && this.address.isObjectEmpty() && isNullOrEmpty(this.organizationName);
  }

  @Override
  public String toCsv() {
    return this.participantId + ";" + this.address.toCsv() + this.phoneNumbers + ";" + this.emails + ";"
        + this.person.toCsv() + this.organizationName + ";";
  }
}
