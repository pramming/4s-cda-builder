package dk.s4.hl7.cda.datacreation;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.APDXmlCodec;
import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ApdXmlCdaGenerator extends DataCreationBase {
  private APDXmlCodec apdXmlCodec = new APDXmlCodec();

  public static void main(String[] args) throws Exception {
    ApdXmlCdaGenerator adpXmlCdaGenerator = new ApdXmlCdaGenerator();
    adpXmlCdaGenerator.readProperty(args);
    adpXmlCdaGenerator.parseCsvFile();
  }

  @Override
  protected void parseCsvFile() throws IOException {
    List<String> apdFullCsvHeader = ApdCsvInputParser.getAllApdCsvHeaders();

    File[] adpCsvFiles = listFilesFromDirectory(generatedApdDataFilesPath);
    for (File adpCsvFile : adpCsvFiles) {
      List<CSVRecord> generatedAdpFilesDateRecords = getCsvRecords(adpCsvFile.getAbsolutePath(), "UTF-8",
          apdFullCsvHeader.toArray(new String[apdFullCsvHeader.size()]));

      for (int recordIndex = 1; recordIndex < generatedAdpFilesDateRecords.size(); recordIndex++) {
        CSVRecord apdRecord = generatedAdpFilesDateRecords.get(recordIndex);
        AppointmentDocument appointmentDocument = buildHeader(apdRecord);
        appointmentDocument = buildApdData(apdRecord, appointmentDocument);
        writeXmlFile(createAdpFromCDA(appointmentDocument), adpCsvFile.getName());
      }

    }
  }

  private AppointmentDocument buildHeader(CSVRecord apdFilesDataRecord) {
    String documentationId = apdFilesDataRecord.get(CsvColumnNames.DOKUMENTATION_ID.getColumnName());
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension(documentationId)
        .setRoot(MedCom.ROOT_OID)
        .build();
    AppointmentDocument appointmentDocument = new AppointmentDocument(idHeader);

    Date documentCreationTime = parseStringToDate(
        apdFilesDataRecord.get(CsvColumnNames.APD_OPRETTELSESTIDSPUNKT.getColumnName()));

    // Cusodian
    String custodianSor = apdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_SOR.getColumnName());
    String custodianStednavn = apdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_STEDNAVN.getColumnName());
    String custodianAfdelingNavn = apdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_AFDELING_NAVN.getColumnName());
    String custodianVejnavn = apdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_VEJ_NAVN.getColumnName());
    String custodianPostnummer = apdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_POSTNUMMER.getColumnName());
    String custodianBy = apdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_BY.getColumnName());

    Patient person = definePatientPersonIdentity(apdFilesDataRecord);

    appointmentDocument.setTitle("Aftale for " + person.getIdValue());
    appointmentDocument.setLanguageCode("da-DK");
    appointmentDocument.setEffectiveTime(documentCreationTime);
    appointmentDocument.setPatient(person);

    // Populate author
    appointmentDocument.setAuthor(new Participant.ParticipantBuilder()
        .setCPR(person.getIdValue())
        .setAddress(person.getAddress())
        .setPersonIdentity(person)
        .setTime(documentCreationTime)
        .build());

    AddressData custodianAdress = new AddressData.AddressBuilder(custodianPostnummer, custodianBy)
        .addAddressLine(custodianAfdelingNavn)
        .addAddressLine(custodianVejnavn)
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();

    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR(custodianSor)
        .setName(custodianStednavn)
        .setAddress(custodianAdress)
        .addTelecom(AddressData.Use.WorkPlace, "tel", "65223344")
        .build();

    appointmentDocument.setCustodian(custodianOrganization);

    return appointmentDocument;
  }

  private AppointmentDocument buildApdData(CSVRecord adpRecord, AppointmentDocument appointmentDocument) {
    appointmentDocument.setAppointmentId(adpRecord.get(CsvColumnNames.APD_ID.getColumnName()));
    appointmentDocument.setIndicationDisplayName(adpRecord.get(CsvColumnNames.APD_AFTALE_EMNE.getColumnName()));
    AppointmentDocument.Status appointmentStatus = AppointmentDocument.Status
        .valueOf(adpRecord.get(CsvColumnNames.APD_STATUS.getColumnName()));
    appointmentDocument.setAppointmentStatus(appointmentStatus);

    Date appointmentStartDate = parseStringToDate(adpRecord.get(CsvColumnNames.APD_AFTALE_START.getColumnName()));
    Date appointmentEndDate = parseStringToDate(adpRecord.get(CsvColumnNames.APD_AFTALE_SLUT.getColumnName()));
    Participant performer = defineAppointmentPerformer(adpRecord);
    Participant author = defineAppointmentAuthor(adpRecord);
    OrganizationIdentity location = defineAppointmentLocation(adpRecord);

    appointmentDocument.setDocumentationTimeInterval(appointmentStartDate, appointmentEndDate);
    appointmentDocument.setAppointmentPerformer(performer);
    appointmentDocument.setAppointmentAuthor(author);
    appointmentDocument.setAppointmentLocation(location);
    appointmentDocument.setAppointmentTitle("Aftale");
    appointmentDocument.setAppointmentText(buildAppointmentText(appointmentDocument));

    return appointmentDocument;
  }

  private void writeXmlFile(String createAdpFromCDA, String fileName) throws IOException {
    String xmlFileName = fileName.replace("csv", "xml");
    String outputFileName = generatedApdXmlFilesPath + xmlFileName;
    new File(outputFileName).getParentFile().mkdirs();
    Writer out = new OutputStreamWriter(new FileOutputStream(outputFileName), "UTF-8");
    out.write(createAdpFromCDA);
    out.flush();
    out.close();
  }

  public String createAdpFromCDA(AppointmentDocument appointmentDocument) {
    return apdXmlCodec.encode(appointmentDocument);
  }

  private String buildAppointmentText(AppointmentDocument appointmentDocument) {
    String status = appointmentDocument.getAppointmentStatus().toString().toLowerCase();
    String displayName = appointmentDocument.getIndicationDisplayName();
    String startTime = UtilXmlGenerator.format(appointmentDocument.getServiceStartTime());
    String endTime = UtilXmlGenerator.format(appointmentDocument.getServiceStopTime());
    String zipCode = appointmentDocument.getAppointmentLocation().getAddress().getPostalCode();
    String city = appointmentDocument.getAppointmentLocation().getAddress().getCity();
    List<String> streetNames = Arrays.asList(appointmentDocument.getAppointmentLocation().getAddress().getStreet());

    String locationStreetName = "";
    for (int i = 0; i < streetNames.size(); i++) {
      if (i > 0) {
        locationStreetName += " ";
      }
      locationStreetName += streetNames.get(i);
    }

    return "<paragraph>Aftale:</paragraph>" + "<table width=\"100%\">" + "<tbody>" + "<tr>" + "<th>Status</th>"
        + "<th>Aftale dato</th>" + "<th>Vedrørende</th>" + "<th>Mødested</th>" + "</tr>" + "<tr>" + "<td>" + status
        + "</td>" + "<td>" + startTime + " - " + endTime + "</td>" + "<td>" + displayName + "</td>" + "<td>"
        + locationStreetName + ", " + zipCode + " " + city + "</td>" + "</tr>" + "</tbody>" + "</table>";
  }

  protected static Participant defineAppointmentAuthor(CSVRecord filesDataRecord) {
    PersonIdentity personIdentity = new PersonIdentity.PersonBuilder()
        .setPrefix(filesDataRecord.get(CsvColumnNames.APD_FORFATTER_PERS_IDENTITY_PRAEFIX.getColumnName()))
        .addFamilyName(filesDataRecord.get(CsvColumnNames.APD_FORFATTER_PERS_IDENTITY_EFTERNAVN.getColumnName()))
        .addGivenName(filesDataRecord.get(CsvColumnNames.APD_FORFATTER_PERS_IDENTITY_FORNAVNE.getColumnName()))
        .build();

    AddressData addressData = new AddressData.AddressBuilder()
        .setCity(filesDataRecord.get(CsvColumnNames.APD_FORFATTER_ADRESSE_BY.getColumnName()))
        .setCountry(filesDataRecord.get(CsvColumnNames.APD_FORFATTER_ADRESSE_LAND.getColumnName()))
        .setPostalCode(filesDataRecord.get(CsvColumnNames.APD_FORFATTER_ADRESSE_POSTNR.getColumnName()))
        .addAddressLine(filesDataRecord.get(CsvColumnNames.APD_FORFATTER_ADRESSE_VEJ.getColumnName()))
        .setUse(AddressData.Use.WorkPlace)
        .build();

    OrganizationIdentity organizationIdentity = new OrganizationIdentity.OrganizationBuilder()
        .setName(filesDataRecord.get(CsvColumnNames.APD_FORFATTER_ORGANISATION_NAVN.getColumnName()))
        .build();

    Date documentCreationTime = parseStringToDate(
        filesDataRecord.get(CsvColumnNames.APD_OPRETTELSESTIDSPUNKT.getColumnName()));
    String phoneValues = filesDataRecord.get(CsvColumnNames.APD_FORFATTER_TLF.getColumnName());
    String mailValues = filesDataRecord.get(CsvColumnNames.APD_FORFATTER_EMAIL.getColumnName());
    List<Telecom> telecoms = parseStringToListOfTelecoms(phoneValues, "tlf", AddressData.Use.WorkPlace);
    telecoms.addAll(parseStringToListOfTelecoms(mailValues, "mailto", AddressData.Use.WorkPlace));

    Participant author = new Participant.ParticipantBuilder()
        .setPersonIdentity(personIdentity)
        .setAddress(addressData)
        .setOrganizationIdentity(organizationIdentity)
        .setTelecomList(telecoms)
        .setTime(documentCreationTime)
        .setSOR(filesDataRecord.get(CsvColumnNames.APD_FORFATTER_ID.getColumnName()))
        .build();

    return author;
  }

  protected static Participant defineAppointmentPerformer(CSVRecord filesDataRecord) {
    PersonIdentity personIdentity = new PersonIdentity.PersonBuilder()
        .setPrefix(filesDataRecord.get(CsvColumnNames.APD_UDFOERER_PERS_IDENTITY_PRAEFIX.getColumnName()))
        .addFamilyName(filesDataRecord.get(CsvColumnNames.APD_UDFOERER_PERS_IDENTITY_EFTERNAVN.getColumnName()))
        .addGivenName(filesDataRecord.get(CsvColumnNames.APD_UDFOERER_PERS_IDENTITY_FORNAVNE.getColumnName()))
        .build();

    AddressData addressData = new AddressData.AddressBuilder()
        .setCity(filesDataRecord.get(CsvColumnNames.APD_UDFOERER_ADRESSE_BY.getColumnName()))
        .setCountry(filesDataRecord.get(CsvColumnNames.APD_UDFOERER_ADRESSE_LAND.getColumnName()))
        .setPostalCode(filesDataRecord.get(CsvColumnNames.APD_UDFOERER_ADRESSE_POSTNR.getColumnName()))
        .addAddressLine(filesDataRecord.get(CsvColumnNames.APD_UDFOERER_ADRESSE_VEJ.getColumnName()))
        .setUse(AddressData.Use.WorkPlace)
        .build();

    OrganizationIdentity organizationIdentity = new OrganizationIdentity.OrganizationBuilder()
        .setAddress(addressData)
        .setName(filesDataRecord.get(CsvColumnNames.APD_UDFOERER_ORGANISATION_NAVN.getColumnName()))
        .build();

    Date documentCreationTime = parseStringToDate(
        filesDataRecord.get(CsvColumnNames.APD_OPRETTELSESTIDSPUNKT.getColumnName()));
    String phoneValues = filesDataRecord.get(CsvColumnNames.APD_UDFOERER_TLF.getColumnName());
    String mailValues = filesDataRecord.get(CsvColumnNames.APD_UDFOERER_EMAIL.getColumnName());
    List<Telecom> telecoms = parseStringToListOfTelecoms(phoneValues, "tlf", AddressData.Use.WorkPlace);
    telecoms.addAll(parseStringToListOfTelecoms(mailValues, "mailto", AddressData.Use.WorkPlace));

    Participant performer = new Participant.ParticipantBuilder()
        .setPersonIdentity(personIdentity)
        .setAddress(addressData)
        .setOrganizationIdentity(organizationIdentity)
        .setTelecomList(telecoms)
        .setTime(documentCreationTime)
        .setSOR(filesDataRecord.get(CsvColumnNames.APD_UDFOERER_ID.getColumnName()))
        .build();

    return performer;
  }

  protected static OrganizationIdentity defineAppointmentLocation(CSVRecord filesDataRecord) {
    AddressData addressData = new AddressData.AddressBuilder()
        .setCity(filesDataRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_BY.getColumnName()))
        .setCountry(filesDataRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_LAND.getColumnName()))
        .setPostalCode(filesDataRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_POSTNR.getColumnName()))
        .addAddressLine(filesDataRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_VEJ.getColumnName()))
        .setUse(AddressData.Use.WorkPlace)
        .build();

    String phoneValues = filesDataRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_TLF.getColumnName());
    String mailValues = filesDataRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_EMAIL.getColumnName());
    List<Telecom> telecoms = parseStringToListOfTelecoms(phoneValues, "tlf", AddressData.Use.WorkPlace);
    telecoms.addAll(parseStringToListOfTelecoms(mailValues, "mailto", AddressData.Use.WorkPlace));

    OrganizationIdentity organizationIdentity = new OrganizationIdentity.OrganizationBuilder()
        .setAddress(addressData)
        .setName(filesDataRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_NAVN.getColumnName()))
        .addTelecoms(telecoms)
        .setSOR(filesDataRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_ID.getColumnName()))
        .build();

    return organizationIdentity;
  }

  private static Date parseStringToDate(String dateAsString) {
    return DateTime.parse(dateAsString).toDate();
  }

  private static List<Telecom> parseStringToListOfTelecoms(String csvValue, String protocol, AddressData.Use use) {
    List<Telecom> listOfTelecoms = new ArrayList<>();
    if (csvValue != null && !csvValue.trim().isEmpty()) {
      List<String> csvValues = Arrays.asList(csvValue.split("#"));
      for (String telecomValue : csvValues) {
        if (telecomValue != null && !telecomValue.trim().isEmpty()) {
          listOfTelecoms.add(new Telecom(use, protocol, telecomValue));
        }
      }
    }
    return listOfTelecoms;
  }

}
