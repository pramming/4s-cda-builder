package dk.s4.hl7.cda.download;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.net4care.xdsconnector.IRegistryConnector;
import org.net4care.xdsconnector.IRepositoryConnector;
import org.net4care.xdsconnector.SendRequestException;
import org.net4care.xdsconnector.Constants.XDSStatusValues.AdhocQueryResponse;
import org.net4care.xdsconnector.service.AdhocQueryResponseType;
import org.net4care.xdsconnector.service.ExternalIdentifierType;
import org.net4care.xdsconnector.service.ExtrinsicObjectType;
import org.net4care.xdsconnector.service.IdentifiableType;
import org.net4care.xdsconnector.service.ObjectFactory;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType.DocumentResponse;
import org.net4care.xdsconnector.service.SlotType1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Downloader {
  private static final String UUID_DOCUMENT_UNIQUE_ID = "urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab";
  private static final Logger logger = LoggerFactory.getLogger(Downloader.class);
  private IRegistryConnector registryConnector;
  private IRepositoryConnector repositoryConnector;
  private DownloaderProperties properties;

  private static final JAXBContext jaxbContext = createJAXBContext();

  private static JAXBContext createJAXBContext() {
    try {
      return JAXBContext.newInstance(AdhocQueryResponseType.class, RetrieveDocumentSetResponseType.class);
    } catch (JAXBException e) {
      throw new RuntimeException(e);
    }
  }

  public Downloader(IRegistryConnector registryConnector, IRepositoryConnector repositoryConnector,
      DownloaderProperties properties) {
    super();
    this.registryConnector = registryConnector;
    this.repositoryConnector = repositoryConnector;
    this.properties = properties;
  }

  public List<DocumentResponse> download(CDAFindDocumentsQueryBuilder queryBuilder) throws SendRequestException {
    logger.info("Sending FindDocuments query to Registry");
    AdhocQueryResponseType registryResponse = registryConnector.executeQuery(queryBuilder.build());
    List<String> documentUniqueIds = processRegistryResponse(registryResponse);
    if (!documentUniqueIds.isEmpty()) {
      logger.info("Sending RetrieveDocumentSet query to Repository");
      RetrieveDocumentSetResponseType repositoryResponse = repositoryConnector.retrieveDocumentSet(documentUniqueIds);
      return processRespositoryResponse(repositoryResponse);
    }
    return new ArrayList<DocumentResponse>();
  }

  private List<DocumentResponse> processRespositoryResponse(RetrieveDocumentSetResponseType repositoryResponse) {
    List<DocumentResponse> documents = new ArrayList<DocumentResponse>();
    if (repositoryResponse.getDocumentResponse() != null && !repositoryResponse.getDocumentResponse().isEmpty()) {
      documents.addAll(repositoryResponse.getDocumentResponse());
    } else {
      printCompleteResponse(repositoryResponse);
      logger.warn("No documents to download");
    }
    return documents;
  }

  private List<String> processRegistryResponse(AdhocQueryResponseType registryResponse) {
    List<String> documentUnqiueIds = new ArrayList<String>();
    if (!AdhocQueryResponse.Failure.equalsIgnoreCase(registryResponse.getStatus())) {
      logger.info(String.format("Found %s metadata documents",
          registryResponse.getRegistryObjectList().getIdentifiable().size()));
      for (JAXBElement<? extends IdentifiableType> identifiable : registryResponse
          .getRegistryObjectList()
          .getIdentifiable()) {
        if (identifiable != null && ExtrinsicObjectType.class.isAssignableFrom(identifiable.getDeclaredType())) {
          ExtrinsicObjectType documentMetadata = (ExtrinsicObjectType) identifiable.getValue();
          String documentUnqiueId = getDocumentUniqueId(documentMetadata);
          String repositoryUniqueId = getRepositoryUniqueId(documentMetadata);
          if (documentUnqiueId != null && repositoryUniqueId != null) {
            if (properties.getRepositoryId().equalsIgnoreCase(repositoryUniqueId)) {
              documentUnqiueIds.add(documentUnqiueId);
            } else {
              logger.warn(
                  "A document is located in a repository which is different from the one configured in properties: "
                      + String.format("configured=%s, metadata=%s", properties.getRepositoryId(), repositoryUniqueId));
            }
          }
        }
      }
      if (documentUnqiueIds.isEmpty()) {
        printCompleteResponse(registryResponse);
        logger.error(
            "The registry FindDocuments query was successful but no usable metadata could be found in the response");
      }
    } else {
      printCompleteResponse(registryResponse);
      logger.error("The registry FindDocuments query was unsuccessful - Please look in the above response");
    }
    return documentUnqiueIds;
  }

  private static String getDocumentUniqueId(ExtrinsicObjectType documentMetadata) {
    for (ExternalIdentifierType externalIdentifier : documentMetadata.getExternalIdentifier()) {
      if (UUID_DOCUMENT_UNIQUE_ID.equalsIgnoreCase(externalIdentifier.getIdentificationScheme())) {
        return externalIdentifier.getValue();
      }
    }
    return null;
  }

  private static String getRepositoryUniqueId(ExtrinsicObjectType documentMetadata) {
    for (SlotType1 slot : documentMetadata.getSlot()) {
      if (slot.getName().equalsIgnoreCase("repositoryUniqueId")) {
        return slot.getValueList().getValue().get(0);
      }
    }
    return null;
  }

  private static void printCompleteResponse(AdhocQueryResponseType response) {
    try {
      StringWriter writer = new StringWriter();
      writer.append("Registry response: ");
      jaxbContext.createMarshaller().marshal(new ObjectFactory().createAdhocQueryResponse(response), writer);
      logger.info(writer.toString());
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  private void printCompleteResponse(RetrieveDocumentSetResponseType repositoryReponse) {
    try {
      StringWriter writer = new StringWriter();
      writer.append("Repository response: ");
      jaxbContext.createMarshaller().marshal(new ObjectFactory().createRetrieveDocumentSetResponse(repositoryReponse),
          writer);
      logger.info(writer.toString());
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }
}
