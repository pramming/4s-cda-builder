package dk.s4.hl7.cda.datacreation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

/**
 * Base class that contains general methods for the DataGenerator classes.
 * 
 */
public abstract class DataGeneratorBase extends DataCreationBase {
  // This contains the header data information for each person
  protected HashMap<String, String> personDataMap = new HashMap<String, String>();
  // headerPerson contains the header information as string for the person
  protected String headerColumnNames = "";

  // Contains the property file configuration
  protected CronDateTimeGenerator cronDateTimeGenerator;

  // If we are generation QRD documents this is set.
  protected boolean generateQrqDocuments;

  // If we are generation PHMR documents this is set.
  protected boolean generatePhmrDocuments;

  protected void parseCsvFile(String filesPath, String outputFileName, boolean isQrd) throws IOException {
    List<CSVRecord> generatedCdaHeaderDataRecords = getHeaderData();
    getHeaderData(generatedCdaHeaderDataRecords);
    getSpecificData(generatedCdaHeaderDataRecords, filesPath, outputFileName);
  }

  protected List<CSVRecord> getHeaderData() throws IOException {
    String generatedCdaHeaderDataPathCsv = generatedCdaHeaderDataPath + "generatedCdaHeaderDataFile.csv";
    List<CSVRecord> generatedCdaHeaderDataRecords = getCsvRecords(generatedCdaHeaderDataPathCsv);
    return generatedCdaHeaderDataRecords;
  }

  /**
   * Sets the personDataMap, first key value is the person id "patient cpr" and
   * the value is the header data, for each row
   * Creates headerPersonAndData, that is the all the header information from
   * person and the specific data header "QRD" or "PHMR"
   * 
   * @param generatedCdaHeaderDataRecords
   * 
   * @throws UnsupportedEncodingException
   * @throws FileNotFoundException
   * @throws IOException
   *
   */
  protected void getHeaderData(List<CSVRecord> generatedCdaHeaderDataRecords) {
    if (!generatedCdaHeaderDataRecords.isEmpty()) {
      int patientCprColumnIndex = readHeader(generatedCdaHeaderDataRecords.get(0));
      readHeaderData(generatedCdaHeaderDataRecords, patientCprColumnIndex);
    }
  }

  private int readHeader(CSVRecord generatedCdaHeaderDataRecord) {
    int patientCprIndex = -1;
    StringBuilder headerPersonAndDataBuilder = new StringBuilder(1024);
    for (int columnIndex = 0; columnIndex < generatedCdaHeaderDataRecord.size(); columnIndex++) {
      headerPersonAndDataBuilder.append(generatedCdaHeaderDataRecord.get(columnIndex)).append(';');
      if (generatedCdaHeaderDataRecord.get(columnIndex).equalsIgnoreCase(CsvColumnNames.PATIENT_CPR.getColumnName())) {
        patientCprIndex = columnIndex;
      }
    }
    headerColumnNames = headerPersonAndDataBuilder.toString();
    return patientCprIndex;
  }

  private void readHeaderData(List<CSVRecord> generatedCdaHeaderDataRecords, int patientCprColumnIndex) {
    StringBuilder personStringBuilder = new StringBuilder(1024);
    // Ignore row 0 because that is the csv header
    for (int rowIndex = 1; rowIndex < generatedCdaHeaderDataRecords.size(); rowIndex++) {
      CSVRecord generatedCdaHeaderDataRecord = generatedCdaHeaderDataRecords.get(rowIndex);
      String patientCpr = "";
      for (int columnIndex = 0; columnIndex < generatedCdaHeaderDataRecord.size(); columnIndex++) {
        personStringBuilder.append(generatedCdaHeaderDataRecord.get(columnIndex)).append(';');
        if (columnIndex == patientCprColumnIndex) {
          patientCpr = generatedCdaHeaderDataRecord.get(patientCprColumnIndex);
        }
      }
      personDataMap.put(patientCpr, personStringBuilder.toString());
      personStringBuilder.setLength(0);
    }
  }

  /**
   * Get specific data from QRD or PHMR and sets the header in dataHeader & the
   * data records in dataSet
   * 
   * @param filesPath
   * @throws UnsupportedEncodingException
   * @throws FileNotFoundException
   * @throws IOException
   */
  protected void getSpecificData(List<CSVRecord> generatedCdaHeaderDataRecords, String filesPath, String outputFileName)
      throws UnsupportedEncodingException, FileNotFoundException, IOException {
    File[] fileListAll = listFilesFromDirectory(filesPath);
    // Pick random data specific template from phmr or qrd templates
    // get random between 0 and list length - 1
    File file = fileListAll[(int) (Math.random() * (fileListAll.length - 1))];

    // Generate specific PHMR or QRD data for each file.
    // if there are many documents for each person we want to create, create
    // random data for this.
    String filesPathCsv = file.getAbsolutePath();
    List<CSVRecord> fileDataRecords = getCsvRecordsWithHeader(filesPathCsv);
    if (!fileDataRecords.isEmpty()) {
      String allCsvColumnNames = createCSVColumnNames(fileDataRecords);
      List<String> dataSet = new ArrayList<String>();
      Set<Entry<String, String>> personDataEntrySet = personDataMap.entrySet();
      Iterator<Entry<String, String>> itr = personDataEntrySet.iterator();
      while (itr.hasNext()) {
        Entry<String, String> personData = itr.next();
        cronDateTimeGenerator.reset();
        for (int counter = 0; cronDateTimeGenerator.hasNext(); counter++) {
          String docId = UUID.randomUUID().toString();
          String docTid = cronDateTimeGenerator.next().toString();
          for (int recordIndex = 1; recordIndex < fileDataRecords.size(); recordIndex++) {
            dataSet.addAll(manipulateData(fileDataRecords.get(recordIndex), docId, docTid));
          }
          writeCsvFile(allCsvColumnNames, personData.getValue(), dataSet,
              outputFileName + personData.getKey() + String.format("_%05d.csv", counter));
          dataSet.clear();
        }
      }
    }
  }

  protected String createCSVColumnNames(List<CSVRecord>... fileDataRecords) {
    String dataColumnNames = copyHeaderFromEachCSV(fileDataRecords);
    StringBuilder csvHeaderBuilder = new StringBuilder(1024);
    csvHeaderBuilder.append(headerColumnNames);
    csvHeaderBuilder.append(dataColumnNames);
    csvHeaderBuilder.append(";dokumentation_id;objekt_id");
    return csvHeaderBuilder.toString();
  }

  protected abstract List<String> manipulateData(CSVRecord csvRecord, String docId, String docTid);

  /**
   * Get the Reader, the is special setup its we want to generate many files and
   * data, then we need the header information
   * 
   * @param reader
   * @return CSVRecord
   * @throws IOException
   */

  protected List<CSVRecord> getCsvRecordsWithHeader(String fileName) throws IOException {
    if (generateQrqDocuments) {
      return getCsvRecords(fileName, "UTF-8", CsvColumnNames.QRD_DOK_TITEL.getColumnName(),
          CsvColumnNames.QRD_SKABELON.getColumnName(), CsvColumnNames.QRD_SPOERGSMAAL_ID.getColumnName(),
          CsvColumnNames.QRD_TYPE.getColumnName(), CsvColumnNames.QRD_SVAR.getColumnName(),
          CsvColumnNames.QRD_TITEL.getColumnName(), CsvColumnNames.QRD_VEJLEDENDE_TEKST.getColumnName(),
          CsvColumnNames.QRD_SPOERGSMAAL_TEXT.getColumnName(), CsvColumnNames.QRD_ANTAL.getColumnName());
    }
    if (generatePhmrDocuments) {
      return getCsvRecords(fileName, "UTF-8", CsvColumnNames.PHMR_TIDSPUNKT.getColumnName(),
          CsvColumnNames.PHMR_TYPE.getColumnName(), CsvColumnNames.PHMR_ENHED.getColumnName(),
          CsvColumnNames.PHMR_VAERDI.getColumnName(), CsvColumnNames.MEDICAL_DEVICE_CODE.getColumnName(),
          CsvColumnNames.MEDICAL_DEVICE_DISPLAYNAME.getColumnName(),
          CsvColumnNames.MANUFACTURER_MODEL_NAME.getColumnName(),
          CsvColumnNames.PHMR_MAALINGER_PATIENT.getColumnName());
    }
    return getCsvRecords(fileName, "UTF-8");
  }

  /**
   * This method creates the csv file when generating QRD data or PHMR data.
   * 
   * @param headerString
   *          contains the String for the header
   * @param HeaderMap
   *          contains patient id, for the file name, and the person record to
   *          be written.
   * @param dataSet
   *          contains the qrd or phmr specific data.
   * @param outputFileName
   *          is the file name, with path.
   * @param documentsPerPatient
   * @param sizeDocumentsPerPatient
   *          how many document for each patient to create
   * @throws IOException
   * 
   */

  protected void writeCsvFile(String headerString, String headerValues, List<String> dataSet, String outputFileName)
      throws IOException {
    new File(outputFileName).getParentFile().mkdirs();

    Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFileName), "UTF-8"));
    CSVFormat outFormat = CSVFormat.EXCEL.withHeader(headerString);
    CSVPrinter outPrinter = new CSVPrinter(new PrintWriter(out), outFormat);

    for (int j = 0; j < dataSet.size(); j++) {
      out.write(headerValues);
      out.write(dataSet.get(j));
      out.write('\n');
    }
    outPrinter.flush();
    outPrinter.close();
  }

  protected String splitData(CSVRecord dataRecord, String docId) {
    StringBuilder csvRecordStringBuilder = new StringBuilder();
    for (int i = 0; i < dataRecord.size(); i++) {
      csvRecordStringBuilder.append(dataRecord.get(i)).append(';');
    }

    String objId = UUID.randomUUID().toString();
    csvRecordStringBuilder.append(docId + ";" + objId + ";");

    if (csvRecordStringBuilder.length() > 0) {
      csvRecordStringBuilder.setLength(csvRecordStringBuilder.length() - 1);
    }

    return csvRecordStringBuilder.toString();
  }
}
