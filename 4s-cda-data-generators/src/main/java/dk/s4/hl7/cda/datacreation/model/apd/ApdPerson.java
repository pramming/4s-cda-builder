package dk.s4.hl7.cda.datacreation.model.apd;

public class ApdPerson extends ApdCsvObject {
  private String prefix;
  private String firstnames;
  private String lastName;

  public String getPrefix() {
    return prefix;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public String getFirstnames() {
    return firstnames;
  }

  public void setFirstnames(String firstnames) {
    this.firstnames = firstnames;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @Override
  public boolean isObjectEmpty() {
    return isNullOrEmpty(this.prefix) && isNullOrEmpty(this.firstnames) && isNullOrEmpty(this.lastName);
  }

  @Override
  public String toCsv() {
    return this.prefix + ";" + this.firstnames + ";" + this.lastName + ";";
  }
}
