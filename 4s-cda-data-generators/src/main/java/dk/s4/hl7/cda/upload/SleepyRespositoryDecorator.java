package dk.s4.hl7.cda.upload;

import java.util.List;

import org.net4care.xdsconnector.IRepositoryConnector;
import org.net4care.xdsconnector.SendRequestException;
import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.Utilities.PrepareRequestException;
import org.net4care.xdsconnector.Utilities.RetrieveDocumentSetRequestTypeBuilder;
import org.net4care.xdsconnector.service.RegistryResponseType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

public class SleepyRespositoryDecorator implements IRepositoryConnector {
  private static final Logger logger = LoggerFactory.getLogger(SleepyRespositoryDecorator.class);
  private IRepositoryConnector nextIRepositoryConnector;
  private final int nofUploadedDocumentsBeforeSleep;
  private final long sleepDuration;
  private int currentNofUploadedDocuments;

  private long invokeStartTime;
  private long invokeDuration;

  public SleepyRespositoryDecorator(int nofUploadedDocumentsBeforeSleep, long sleepDuration,
      IRepositoryConnector nextIRepositoryConnector) {
    super();
    this.nextIRepositoryConnector = nextIRepositoryConnector;
    this.nofUploadedDocumentsBeforeSleep = nofUploadedDocumentsBeforeSleep;
    this.sleepDuration = sleepDuration;
    this.currentNofUploadedDocuments = 0;
    this.invokeDuration = 0;
  }

  @Override
  public RetrieveDocumentSetResponseType retrieveDocumentSet(RetrieveDocumentSetRequestTypeBuilder builder)
      throws SendRequestException {
    beforeInvoke();
    RetrieveDocumentSetResponseType retrievedDocuments = nextIRepositoryConnector.retrieveDocumentSet(builder);
    if (retrievedDocuments != null && retrievedDocuments.getDocumentResponse() != null) {
      afterInvoke(retrievedDocuments.getDocumentResponse().size());
    }
    return retrievedDocuments;
  }

  @Override
  public RetrieveDocumentSetResponseType retrieveDocumentSet(String docId) throws SendRequestException {
    beforeInvoke();
    RetrieveDocumentSetResponseType retrievedDocuments = nextIRepositoryConnector.retrieveDocumentSet(docId);
    if (retrievedDocuments != null && retrievedDocuments.getDocumentResponse() != null) {
      afterInvoke(retrievedDocuments.getDocumentResponse().size());
    }
    return retrievedDocuments;
  }

  @Override
  public RetrieveDocumentSetResponseType retrieveDocumentSet(List<String> docIds) throws SendRequestException {
    beforeInvoke();
    RetrieveDocumentSetResponseType retrievedDocuments = nextIRepositoryConnector.retrieveDocumentSet(docIds);
    if (retrievedDocuments != null && retrievedDocuments.getDocumentResponse() != null) {
      afterInvoke(retrievedDocuments.getDocumentResponse().size());
    }
    return retrievedDocuments;
  }

  @Override
  public RegistryResponseType provideAndRegisterCDADocument(Document cda, CodedValue healthcareFacilityType,
      CodedValue practiceSettingsCode) throws PrepareRequestException, SendRequestException {
    beforeInvoke();
    RegistryResponseType response = nextIRepositoryConnector.provideAndRegisterCDADocument(cda, healthcareFacilityType,
        practiceSettingsCode);
    afterInvoke(1);
    return response;
  }

  @Override
  public RegistryResponseType provideAndRegisterCDADocument(String cda, CodedValue healthcareFacilityType,
      CodedValue practiceSettingsCode) throws PrepareRequestException, SendRequestException {
    beforeInvoke();
    RegistryResponseType response = nextIRepositoryConnector.provideAndRegisterCDADocument(cda, healthcareFacilityType,
        practiceSettingsCode);
    afterInvoke(1);
    return response;
  }

  @Override
  public RegistryResponseType provideAndRegisterCDADocuments(List<String> cdas, CodedValue healthcareFacilityType,
      CodedValue practiceSettingsCode) throws PrepareRequestException, SendRequestException {
    beforeInvoke();
    RegistryResponseType response = nextIRepositoryConnector.provideAndRegisterCDADocuments(cdas,
        healthcareFacilityType, practiceSettingsCode);
    afterInvoke(cdas.size());
    return response;
  }

  private void beforeInvoke() {
    if (currentNofUploadedDocuments >= nofUploadedDocumentsBeforeSleep) {
      sleep(sleepDuration - invokeDuration);
    }
    invokeStartTime = System.currentTimeMillis();
  }

  private void afterInvoke(int countUploadedDocuments) {
    invokeDuration = System.currentTimeMillis() - invokeStartTime;
    if (currentNofUploadedDocuments >= nofUploadedDocumentsBeforeSleep) {
      currentNofUploadedDocuments = 0;
    }
    currentNofUploadedDocuments += countUploadedDocuments;
  }

  private void sleep(long sleep) {
    try {
      if (sleep > 0) {
        logger.info("Sleeping: " + sleep + " ms");
        Thread.sleep(sleep);
      } else {
        logger.info("No time for sleep!");
      }
    } catch (InterruptedException e) {
      logger.error(e.getMessage(), e);
    }
  }
}
