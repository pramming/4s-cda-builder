package dk.s4.hl7.cda.datacreation;

import java.util.List;

/**
 * 
 * RingBuffer that runs through a list in a ring.
 * Makes it possible to skip the first element in the list which
 * is useful for csv input files where the first row is the header
 *
 * @param <E>
 */
public class RingBuffer<E> {
  private List<E> list;
  private int currentPosition;
  private boolean skipFirst;

  public RingBuffer(List<E> list, boolean skipFirst) {
    this.list = list;
    this.currentPosition = list.size() - 1; // Will be overriden to 0 on first run
    this.skipFirst = skipFirst;
  }

  public E next() {
    if (++currentPosition >= list.size()) {
      currentPosition = skipFirst ? 1 : 0;
    }
    return list.get(currentPosition);
  }

}
