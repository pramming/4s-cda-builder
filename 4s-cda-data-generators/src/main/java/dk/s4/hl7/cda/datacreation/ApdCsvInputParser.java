package dk.s4.hl7.cda.datacreation;

import org.apache.commons.csv.CSVRecord;

import dk.s4.hl7.cda.datacreation.model.apd.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ApdCsvInputParser {

  public ApdCsvInputParser() {
  }

  public static CSVRecord getRandomCvsRecord(List<CSVRecord> csvRecords) {
    int randomIndex = (int) (Math.random() * (csvRecords.size() - 1)) + 1;
    return csvRecords.get(randomIndex);
  }

  public static ApdBase getApdBaseFromCsvRecord(CSVRecord csvRecord) {
    ApdBase apdBase = new ApdBase();

    String appointmentId = csvRecord.get(CsvColumnNames.APD_ID.getColumnName());
    if (!isNullOrEmpty(appointmentId)) {
      apdBase.setAppointmentId(appointmentId);
    } else {
      apdBase.setAppointmentId(UUID.randomUUID().toString());
    }
    apdBase.setAppointmentStatus(csvRecord.get(CsvColumnNames.APD_STATUS.getColumnName()));
    apdBase.setAppointmentCreationDate(csvRecord.get(CsvColumnNames.APD_OPRETTELSESTIDSPUNKT.getColumnName()));
    return apdBase;
  }

  public static ApdTime getApdTimeFromCsvRecord(CSVRecord csvRecord) {
    ApdTime apdTime = new ApdTime();
    apdTime.setAppointmentStart(csvRecord.get(CsvColumnNames.APD_AFTALE_START.getColumnName()));
    apdTime.setAppointmentEnd(csvRecord.get(CsvColumnNames.APD_AFTALE_SLUT.getColumnName()));
    apdTime.setAppointmentCron(csvRecord.get(CsvColumnNames.APD_CRON.getColumnName()));
    apdTime.setAppointmentDuration(csvRecord.get(CsvColumnNames.APD_CRON_AFTALELAENGDE.getColumnName()));
    return apdTime;
  }

  public static ApdReason getApdReasonFromCsvRecord(CSVRecord csvRecord) {
    ApdReason apdReason = new ApdReason();
    apdReason.setIndicationDisplayName(csvRecord.get(CsvColumnNames.APD_AFTALE_EMNE.getColumnName()));
    ApdParticipant performer = getApdPerformerFromCsvRecord(csvRecord);
    ApdOrganization location = getApdLocationFromCsvRecord(csvRecord);
    apdReason.setPerformer(performer);
    apdReason.setLocation(location);
    return apdReason;
  }

  public static ApdOrganization getApdLocationFromCsvRecord(CSVRecord csvRecord) {
    ApdOrganization apdLocation = new ApdOrganization();
    apdLocation.setOrganizationId(csvRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_ID.getColumnName()));
    apdLocation.setName(csvRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_NAVN.getColumnName()));
    apdLocation.setPhoneNumbers(csvRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_TLF.getColumnName()));
    apdLocation.setEmails(csvRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_EMAIL.getColumnName()));

    ApdAddress address = new ApdAddress();
    address.setStreetNames(csvRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_VEJ.getColumnName()));
    address.setPostalCode(csvRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_POSTNR.getColumnName()));
    address.setCity(csvRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_BY.getColumnName()));
    address.setCountry(csvRecord.get(CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_LAND.getColumnName()));
    apdLocation.setAddress(address);

    return apdLocation;
  }

  public static ApdParticipant getApdPerformerFromCsvRecord(CSVRecord csvRecord) {
    ApdParticipant performer = new ApdParticipant();
    performer.setParticipantId(csvRecord.get(CsvColumnNames.APD_UDFOERER_ID.getColumnName()));
    performer.setPhoneNumbers(csvRecord.get(CsvColumnNames.APD_UDFOERER_TLF.getColumnName()));
    performer.setEmails(csvRecord.get(CsvColumnNames.APD_UDFOERER_EMAIL.getColumnName()));
    performer.setOrganizationName(csvRecord.get(CsvColumnNames.APD_UDFOERER_ORGANISATION_NAVN.getColumnName()));

    ApdAddress address = new ApdAddress();
    address.setStreetNames(csvRecord.get(CsvColumnNames.APD_UDFOERER_ADRESSE_VEJ.getColumnName()));
    address.setPostalCode(csvRecord.get(CsvColumnNames.APD_UDFOERER_ADRESSE_POSTNR.getColumnName()));
    address.setCity(csvRecord.get(CsvColumnNames.APD_UDFOERER_ADRESSE_BY.getColumnName()));
    address.setCountry(csvRecord.get(CsvColumnNames.APD_UDFOERER_ADRESSE_LAND.getColumnName()));
    performer.setAddress(address);

    ApdPerson person = new ApdPerson();
    person.setPrefix(csvRecord.get(CsvColumnNames.APD_UDFOERER_PERS_IDENTITY_PRAEFIX.getColumnName()));
    person.setFirstnames(csvRecord.get(CsvColumnNames.APD_UDFOERER_PERS_IDENTITY_FORNAVNE.getColumnName()));
    person.setLastName(csvRecord.get(CsvColumnNames.APD_UDFOERER_PERS_IDENTITY_EFTERNAVN.getColumnName()));
    performer.setPerson(person);

    return performer;
  }

  public static ApdParticipant getApdAuthorFromCsvRecord(CSVRecord csvRecord) {
    ApdParticipant author = new ApdParticipant();
    author.setParticipantId(csvRecord.get(CsvColumnNames.APD_FORFATTER_ID.getColumnName()));
    author.setPhoneNumbers(csvRecord.get(CsvColumnNames.APD_FORFATTER_TLF.getColumnName()));
    author.setEmails(csvRecord.get(CsvColumnNames.APD_FORFATTER_EMAIL.getColumnName()));
    author.setOrganizationName(csvRecord.get(CsvColumnNames.APD_FORFATTER_ORGANISATION_NAVN.getColumnName()));

    ApdAddress address = new ApdAddress();
    address.setStreetNames(csvRecord.get(CsvColumnNames.APD_FORFATTER_ADRESSE_VEJ.getColumnName()));
    address.setPostalCode(csvRecord.get(CsvColumnNames.APD_FORFATTER_ADRESSE_POSTNR.getColumnName()));
    address.setCity(csvRecord.get(CsvColumnNames.APD_FORFATTER_ADRESSE_BY.getColumnName()));
    address.setCountry(csvRecord.get(CsvColumnNames.APD_FORFATTER_ADRESSE_LAND.getColumnName()));
    author.setAddress(address);

    ApdPerson person = new ApdPerson();
    person.setPrefix(csvRecord.get(CsvColumnNames.APD_FORFATTER_PERS_IDENTITY_PRAEFIX.getColumnName()));
    person.setFirstnames(csvRecord.get(CsvColumnNames.APD_FORFATTER_PERS_IDENTITY_FORNAVNE.getColumnName()));
    person.setLastName(csvRecord.get(CsvColumnNames.APD_FORFATTER_PERS_IDENTITY_EFTERNAVN.getColumnName()));
    author.setPerson(person);

    return author;
  }

  public static List<String> getAllApdCsvHeaders() {
    List<String> apdFullCsvHeaders = new ArrayList<>();
    apdFullCsvHeaders.addAll(getCdaHeaderCsvHeaders());
    apdFullCsvHeaders.addAll(getApdBaseCsvHeaders());
    apdFullCsvHeaders.addAll(getApdAuthorCsvHeaders());
    apdFullCsvHeaders.add(CsvColumnNames.DOKUMENTATION_ID.getColumnName());
    apdFullCsvHeaders.add(CsvColumnNames.OBJEKT_ID.getColumnName());
    return apdFullCsvHeaders;
  }

  public static List<String> getCdaHeaderCsvHeaders() {
    return Arrays.asList(CsvColumnNames.ORGANISATION_AFDELING_SOR.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_STEDNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_VEJNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_POSTNUMMER.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_BY.getColumnName(),
        CsvColumnNames.ORGANISATION_ANSVARLIG_FORNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_ANSVARLIG_EFTERNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_SOR.getColumnName(), CsvColumnNames.ORGANISATION_NAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_SOR.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_STEDNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_VEJNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_POSTNUMMER.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_BY.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_ANSVARLIG_FORNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_ANSVARLIG_EFTERNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_SOR.getColumnName(), CsvColumnNames.LEGAL_AUTHENTICATOR_NAVN.getColumnName(),
        CsvColumnNames.CUSTODIAN_SOR.getColumnName(), CsvColumnNames.CUSTODIAN_STEDNAVN.getColumnName(),
        CsvColumnNames.CUSTODIAN_AFDELING_NAVN.getColumnName(), CsvColumnNames.CUSTODIAN_VEJ_NAVN.getColumnName(),
        CsvColumnNames.CUSTODIAN_POSTNUMMER.getColumnName(), CsvColumnNames.CUSTODIAN_BY.getColumnName(),
        CsvColumnNames.PATIENT_CPR.getColumnName(), CsvColumnNames.PATIENT_FORNANVN.getColumnName(),
        CsvColumnNames.PATIENT_EFTERNAVN.getColumnName(), CsvColumnNames.PATIENT_VEJNAVN.getColumnName(),
        CsvColumnNames.PATIENT_POSTNUMMER.getColumnName(), CsvColumnNames.PATIENT_BY.getColumnName(),
        CsvColumnNames.PATIENT_TLF.getColumnName(), CsvColumnNames.PATIENT_MAIL.getColumnName(),
        CsvColumnNames.REFERENCER.getColumnName());
  }

  public static List<String> getApdBaseCsvHeaders() {
    List<String> apdBaseCsvHeaders = new ArrayList<>();
    apdBaseCsvHeaders.add(CsvColumnNames.APD_ID.getColumnName());
    apdBaseCsvHeaders.add(CsvColumnNames.APD_STATUS.getColumnName());
    apdBaseCsvHeaders.add(CsvColumnNames.APD_OPRETTELSESTIDSPUNKT.getColumnName());
    apdBaseCsvHeaders.addAll(getApdTimeCsvHeaders());
    apdBaseCsvHeaders.addAll(getApdReasonCsvHeaders());
    return apdBaseCsvHeaders;
  }

  public static List<String> getApdTimeCsvHeaders() {
    return Arrays.asList(CsvColumnNames.APD_AFTALE_START.getColumnName(),
        CsvColumnNames.APD_AFTALE_SLUT.getColumnName(), CsvColumnNames.APD_CRON.getColumnName(),
        CsvColumnNames.APD_CRON_AFTALELAENGDE.getColumnName());
  }

  public static List<String> getApdReasonCsvHeaders() {
    return Arrays.asList(CsvColumnNames.APD_AFTALE_EMNE.getColumnName(),
        CsvColumnNames.APD_LOKATION_ORGANISATION_ID.getColumnName(),
        CsvColumnNames.APD_LOKATION_ORGANISATION_NAVN.getColumnName(),
        CsvColumnNames.APD_LOKATION_ORGANISATION_TLF.getColumnName(),
        CsvColumnNames.APD_LOKATION_ORGANISATION_EMAIL.getColumnName(),
        CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_VEJ.getColumnName(),
        CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_POSTNR.getColumnName(),
        CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_BY.getColumnName(),
        CsvColumnNames.APD_LOKATION_ORGANISATION_ADRESSE_LAND.getColumnName(),
        CsvColumnNames.APD_UDFOERER_ID.getColumnName(), CsvColumnNames.APD_UDFOERER_ADRESSE_VEJ.getColumnName(),
        CsvColumnNames.APD_UDFOERER_ADRESSE_POSTNR.getColumnName(),
        CsvColumnNames.APD_UDFOERER_ADRESSE_BY.getColumnName(),
        CsvColumnNames.APD_UDFOERER_ADRESSE_LAND.getColumnName(), CsvColumnNames.APD_UDFOERER_TLF.getColumnName(),
        CsvColumnNames.APD_UDFOERER_EMAIL.getColumnName(),
        CsvColumnNames.APD_UDFOERER_PERS_IDENTITY_PRAEFIX.getColumnName(),
        CsvColumnNames.APD_UDFOERER_PERS_IDENTITY_FORNAVNE.getColumnName(),
        CsvColumnNames.APD_UDFOERER_PERS_IDENTITY_EFTERNAVN.getColumnName(),
        CsvColumnNames.APD_UDFOERER_ORGANISATION_NAVN.getColumnName());
  }

  public static List<String> getApdAuthorCsvHeaders() {
    return Arrays.asList(CsvColumnNames.APD_FORFATTER_ID.getColumnName(),
        CsvColumnNames.APD_FORFATTER_ADRESSE_VEJ.getColumnName(),
        CsvColumnNames.APD_FORFATTER_ADRESSE_POSTNR.getColumnName(),
        CsvColumnNames.APD_FORFATTER_ADRESSE_BY.getColumnName(),
        CsvColumnNames.APD_FORFATTER_ADRESSE_LAND.getColumnName(), CsvColumnNames.APD_FORFATTER_TLF.getColumnName(),
        CsvColumnNames.APD_FORFATTER_EMAIL.getColumnName(),
        CsvColumnNames.APD_FORFATTER_PERS_IDENTITY_PRAEFIX.getColumnName(),
        CsvColumnNames.APD_FORFATTER_PERS_IDENTITY_FORNAVNE.getColumnName(),
        CsvColumnNames.APD_FORFATTER_PERS_IDENTITY_EFTERNAVN.getColumnName(),
        CsvColumnNames.APD_FORFATTER_ORGANISATION_NAVN.getColumnName());
  }

  private static boolean isNullOrEmpty(String value) {
    return value == null || value.toLowerCase().equals("null") || value.equals("");
  }
}
