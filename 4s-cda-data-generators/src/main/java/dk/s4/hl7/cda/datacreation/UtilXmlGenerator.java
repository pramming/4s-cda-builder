package dk.s4.hl7.cda.datacreation;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilXmlGenerator {

  protected static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

  /**
   * Gets a formatted string representation of the date
   * @param date
   * @return formatted string representation of the date
   */
  public static String format(Date date) {
    return dateFormat.format(date);
  }
}
