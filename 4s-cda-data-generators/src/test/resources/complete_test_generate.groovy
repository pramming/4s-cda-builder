import groovy.io.FileType;

File file = new File("./");

def runProcessStep = { File mainName, String main, String parameter ->
  println "Running ${main} ${parameter}"
  if (parameter != null) {
  def process = "java -cp ${mainName.getName()} ${main} ${parameter}".execute(null, mainName.getParentFile());
      process.in.eachLine { line ->               
        println line;                      
    }
      process.err.eachLine { line ->
        println line;
    }
  } else {
  def process = "java -cp ${mainName.getName()} ${main}".execute(null, mainName.getParentFile());
      process.in.eachLine { line ->               
        println line;                      
    }
      process.err.eachLine { line ->               
        println line;                      
    }
  }
}

file.traverse(type: FileType.FILES, nameFilter: ~/.*-jar-with-dependencies\.jar/) {
  println "Found file ${it.getName()}"
  runProcessStep.call(it, "dk.s4.hl7.cda.datacreation.CdaDataGenerator", "./datacreation/");
  runProcessStep.call(it, "dk.s4.hl7.cda.datacreation.QrdDataGenerator", "./datacreation/");
  runProcessStep.call(it, "dk.s4.hl7.cda.datacreation.PhmrDataGenerator", "./datacreation/");
  runProcessStep.call(it, "dk.s4.hl7.cda.datacreation.ApdDataGenerator", "./datacreation/");
  runProcessStep.call(it, "dk.s4.hl7.cda.datacreation.QrdXmlCdaGenerator", "./datacreation/");
  runProcessStep.call(it, "dk.s4.hl7.cda.datacreation.PhmrXmlCdaGenerator", "./datacreation/");
  runProcessStep.call(it, "dk.s4.hl7.cda.datacreation.ApdXmlCdaGenerator", "./datacreation/");
  }