import groovy.io.FileType;

File file = new File("./datacreation/output");

file.traverse(type: FileType.FILES, nameFilter: ~/.*(\.csv|\.xml|.CSV|.XML)/) {
  println "Deleting ${it.getName()}"
  it.delete();
}