package dk.s4.hl7.cda.upload;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileLoaderUtil {
  public static String loadFile(String fileName) throws Exception {
    InputStream stream = FileLoaderUtil.class.getClassLoader().getResourceAsStream(fileName);
    try {
      BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
      StringBuilder builder = new StringBuilder();
      while (reader.ready()) {
        builder.append(reader.readLine()).append('\n');
      }
      return builder.toString();
    } catch (Exception ex) {
      if (stream != null) {
        try {
          stream.close();
        } catch (IOException e) {
          // Ignore
        }
      }
      throw ex;
    }
  }
}
